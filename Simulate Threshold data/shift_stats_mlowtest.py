# -*- coding: utf-8 -*-
"""
Created on Mon Oct  7 18:33:21 2019

@author: gomel
"""

import scipy.stats as st
import numpy as np
import matplotlib.pyplot as plt
import scipy.special as sp
from matplotlib.animation import FuncAnimation,FFMpegFileWriter
#geofs idea jump transition
import threshold
import metrics as metrics 
import astropy.stats as astrost


def noisytrans(x,j,s):
  noiz = np.random.normal(0, s, 1)
  if x<=j: 
    return 4+noiz[0]
  if x>j:
    return 16+noiz[0]

def noisytrans_erf(x,j,s,s2):
  noiz = np.random.normal(0, s, 1)
  return 4*(sp.erf((x-j)/(np.sqrt(2)*s2))+1)+4+noiz[0]

z=np.arange(-10,10,0.001)

data=np.zeros_like(z)
for i,d in enumerate(z):
  data[i]=noisytrans(d,0,0.1)
plt.figure()
plt.plot(z,data)


sigma=0.5


fig2=plt.figure()
fig2.suptitle("Going Through a threshold.", fontsize=10, fontweight='bold')
mus=np.linspace(-1,1,5)

for j,mu in enumerate(mus):
    ax2 = fig2.add_subplot(len(mus),1,j+1)
    x=np.random.normal(mu, sigma, 1900)
    data2=np.zeros_like(x)
    for i,d in enumerate(x):
      data2[i]=noisytrans(d,0,0.1)    
    plt.hist(data2,bins=80,alpha=0.6,label='avg(x):'+str(mu))
    ax2.legend()
    if j+1<len(mus):
        ax2.axes.get_xaxis().set_visible(False)
    ax2.set_xlim((3,18))


#%%

fig4, ax = plt.subplots()
xdata, ydata = [], []

ln, = plt.plot([], [], 'r', animated=True)
f = np.linspace(-20, 20, 200)

z=f
data=np.zeros_like(z)
for i,d in enumerate(z):
  data[i]=noisytrans(d,0,0.03)
ax.plot(z,data)


##%%
#
#mu_start=-10
#mu_end=10
#f = np.linspace(mu_start, mu_end, 30)
#sigma=0.5
#x=np.random.normal(f, sigma, 900)
#ln, = plt.hist([], 'r', animated=True)
#
#xdata, ydata = [], []
#
#ln, = plt.plot([], [], 'r', animated=True)
#f = np.linspace(-20, 20, 200)
#
#z=f
#data=np.zeros_like(z)
#for i,d in enumerate(z):
#  data[i]=noisytrans(d,0,0.03)
#ax.plot(z,data)


#%%


fig4=plt.figure()
ax = fig4.add_subplot(2,1,1)
ax2 = fig4.add_subplot(2,1,2)

data = []
xdata, ydata = [], []

ln1, = plt.plot([], 'r', animated=True)
f = np.linspace(-20, 20, 200)

z=f
data=np.zeros_like(z)
for i,d in enumerate(z):
  data[i]=noisytrans(d,0,0.03)
ax.plot(z,data)

#%%
#''' other stats''''


z=np.arange(-10,10,0.001)

data=np.zeros_like(z)
for i,d in enumerate(z):
  data[i]=noisytrans_erf(d,0,0.03,1)
plt.figure()
plt.plot(z,data)


sigma=0.5


fig2=plt.figure()
fig2.suptitle("Going Through a threshold.", fontsize=10, fontweight='bold')
mus=np.linspace(-3,3,7)

for j,mu in enumerate(mus):
    ax2 = fig2.add_subplot(len(mus),1,j+1)
    x=np.random.normal(mu, sigma, 1900)
    data2=np.zeros_like(x)
    for i,d in enumerate(x):
      data2[i]=noisytrans_erf(d,0,0.03,1)    
    height,bin_pos=astrost.histogram(data2,bins='scott',density=True)
    ax2.bar(bin_pos[:-1],height,width=np.diff(bin_pos),align='edge')
#    plt.hist(data2,bins=30,alpha=0.6,label='avg(x)'+str(mu))
    ax2.legend()

    if j+1<len(mus):
        ax2.axes.get_xaxis().set_visible(False)
    ax2.set_xlim((3,14))

#%%
z=np.arange(-6,6,0.01)


fig2=plt.figure()
fig2.suptitle("Going Through a threshold - Metrics (5000 points).", fontsize=10, fontweight='bold')
ax2 = fig2.add_subplot(111)
data=np.zeros_like(z)
for i,d in enumerate(z):
  data[i]=noisytrans_erf(d,0,0.03,1)
ax2.plot(z,data,alpha=0.7)

k=0.2

sigma=0.5
mus=np.linspace(-6,6,70)
M=np.zeros_like(mus)
M_low=np.zeros_like(mus)
M_mixed=np.zeros_like(mus)
M_max=np.zeros_like(mus)
rri=np.zeros_like(mus)
kurtis=np.zeros_like(mus)

for j,mu in enumerate(mus):
#    ax2 = fig2.add_subplot(len(mus),1,j+1)
    x=np.random.normal(mu, sigma, 9000)
    data2=np.zeros_like(x)
    for i,d in enumerate(x):
      data2[i]=noisytrans_erf(d,0,0.03,1)    
    
    M[j]=metrics.metricm_norm(data2,k)
    M_low[j]=metrics.metricm_norm_low(data2,k)
    M_mixed[j]=metrics.metricm_norm_mix(data2,k)
    M_max[j]=metrics.metricm_norm_max(data2,k)
    rri[j]=metrics.metric_rc_tld(data2)
    kurtis[j]=st.kurtosis(data2)
#    plt.hist(data2,bins=30,alpha=0.6,label='avg(x)'+str(mu))
#    ax2.legend()
ax3 = ax2.twinx()  # instantiate a second axes that shares the same x-axis
#
#ax3.plot(mus,M,'r',label='Pareto  Normalized high')
#ax3.plot(mus,M_low,'g',label='Pareto  Normalized low')
ax3.plot(mus,M_mixed,'--k',label='Pareto  Normalized avg')
ax3.plot(mus,M_max,'--m',label='Pareto  Normalized max')
plt.legend()
ax3.set_ylabel('Pareto Normalized')
ax3.yaxis.label.set_color('red')
ax3.tick_params(axis='y', colors='red')
#%%
fig2=plt.figure()
fig2.suptitle("Going Through a threshold - Metrics (5000 points).", fontsize=10, fontweight='bold')
ax2 = fig2.add_subplot(111)
ax2.plot(z,data,alpha=0.7)
ax3 = ax2.twinx()  # instantiate a second axes that shares the same x-axis
ax3.plot(mus,kurtis,'orange',label='kurtosis')
ax3.set_ylabel('Kurtosis')
ax3.yaxis.label.set_color('orange')
ax3.tick_params(axis='y', colors='orange')
plt.legend()
fig2=plt.figure()
fig2.suptitle("Going Through a threshold - Metrics (5000 points).", fontsize=10, fontweight='bold')
ax2 = fig2.add_subplot(111)
ax2.plot(z,data)
ax3 = ax2.twinx()  # instantiate a second axes that shares the same x-axis
ax3.plot(mus,rri,'g',label='rri')
ax3.set_ylabel('RRI')


#%%
fig2=plt.figure()
fig2.suptitle("Going Through a threshold - Metrics (5000 points). Pareto vs Kurtosis", fontsize=10, fontweight='bold')
ax2 = fig2.add_subplot(111)
ax2.plot(mus,M,'r',label='Pareto  Normalized')
ax2.set_ylabel('Pareto Normalized')
ax2.yaxis.label.set_color('red')
ax2.tick_params(axis='y', colors='red')
ax3 = ax2.twinx()  # instantiate a second axes that shares the same x-axis
ax3.plot(mus,kurtis,'orange',label='kurtosis')
ax3.set_ylabel('Kurtosis')
ax3.yaxis.label.set_color('orange')
ax3.tick_params(axis='y', colors='orange')
ax4 = ax2.twinx()  # instantiate a second axes that shares the same x-axis
ax4.plot(mus,rri,label='rri')
ax4.yaxis.label.set_color('black')
ax4.tick_params(axis='y', colors='black')
plt.legend()
#%%
fig2=plt.figure()
fig2.suptitle("Going Through a threshold - Metrics (5000 points). Pareto vs Kurtosis", fontsize=10, fontweight='bold')
ax2 = fig2.add_subplot(111)
ax2.plot(mus,M_max,'r',label='Pareto Max')
ax2.set_ylabel('Pareto Normalized')
ax2.yaxis.label.set_color('red')
ax2.tick_params(axis='y', colors='red')
#plt.legend()
ax3 = ax2.twinx()  # instantiate a second axes that shares the same x-axis
ax3.plot(mus,kurtis,'orange',label='kurtosis')
ax3.set_ylabel('Kurtosis')
ax3.yaxis.label.set_color('orange')
ax3.tick_params(axis='y', colors='orange')
#plt.legend()
#%%
fig2=plt.figure()
fig2.suptitle("Going Through a threshold - Metrics (5000 points).", fontsize=10, fontweight='bold')
ax2 = fig2.add_subplot(111)
ax2.plot(z,data,alpha=0.7)
ax3 = ax2.twinx()  # instantiate a second axes that shares the same x-axis
#
#ax3.plot(mus,M,'r',label='Pareto  Normalized high')
ax3.plot(mus,kurtis/max(kurtis),color='orange',label='kurtosis')
ax3.plot(mus,rri/max(rri),'g',label='RRI')
ax3.plot(mus,M_max/max(M_max),'m',label='Pareto  Normalized max')
plt.legend()
ax3.set_ylabel('Metrics Normalized')
#ax3.yaxis.label.set_color('red')
#ax3.tick_params(axis='y', colors='red')
