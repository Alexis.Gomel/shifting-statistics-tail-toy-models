# -*- coding: utf-8 -*-
"""
Created on Tue Jul 21 13:01:26 2020

Transition explanation plot


@author: gomel
"""


import matplotlib.pyplot as plt
import numpy as np
import scipy.stats as st
import expdef
import metrics
# import scipy.special as sp
import astropy.stats as astrost

import matplotlib as mp

import scipy.special as sp
#import astropy.stats.knuth_bin_width as knuth
from scipy.interpolate import griddata
from mpl_toolkits.mplot3d import Axes3D 
import os 
import datetime as dt
from mpl_toolkits.mplot3d.art3d import Poly3DCollection # New import


range_vals=14
sigma=0.4
s2=1
sigma_n0=1.4#2.2
datalen=4000
fig2=plt.figure()
fig2.suptitle("Distributions along the transition. %.1i data points" %(datalen), fontsize=10, fontweight='bold')
mus=np.linspace(-range_vals,range_vals,9)
method='blocks'#blocks
for j,mu in enumerate(mus):
    ax2 = fig2.add_subplot(len(mus),1,j+1)
    x=np.random.normal(mu, sigma, datalen)
    data2=np.zeros_like(x)
    for i,d in enumerate(x):
      data2[i]=expdef.noisytrans_erf(d,0,sigma_n0,s2)    
    height,bin_pos=astrost.histogram(data2,bins=method,density=True)
    ax2.bar(bin_pos[:-1],height,width=np.diff(bin_pos),color='grey',align='edge',label='$\mu$: '+str(mu))
#    plt.hist(data2,bins=30,alpha=0.6,label='avg(x)'+str(mu))
    # ax2.legend()
   
    if j+1<len(mus):
        ax2.axes.get_xaxis().set_visible(False)    
    ax2.set_xlim((4-7*sigma_n0,8+7*sigma_n0))
    ax2.set_yscale('log')
    plt.xticks([4,8],['$y_0$','$y_0+2A_0$'])
    plt.yticks([])
plt.annotate('$\mu$',
      xy=(-0.1,-0.1),xycoords='axes fraction',fontsize='x-large',
      xytext=(-0.1,10),
      arrowprops=dict(arrowstyle="->"),va="center")    
#%%
print('sigma_n0/A0=',sigma_n0/2,'\n'+'sigma/s2=',sigma/s2)
#%%
k=0.2
mus=np.linspace(-range_vals,range_vals,70)
M=np.zeros_like(mus)
M_low=np.zeros_like(mus)
M_mixed=np.zeros_like(mus)
M_max=np.zeros_like(mus)
rc=np.zeros_like(mus)
rri=np.zeros_like(mus)
kurtis=np.zeros_like(mus)
z=np.arange(-range_vals,range_vals,0.01)

for j,mu in enumerate(mus):
    x=np.random.normal(mu, sigma, 9000)
    data2=np.zeros_like(x)
    for i,d in enumerate(x):
      data2[i]=expdef.noisytrans_erf(d,0,sigma_n0,s2)        
    M[j]=metrics.metricm_norm(data2,k)
    M_low[j]=metrics.metricm_norm_low(data2,k)
    M_mixed[j]=metrics.metricm_norm_mix(data2,k)
    M_max[j]=metrics.RTW_max(data2,k)
    rc[j]=metrics.metric_rc_tld(data2)
    rri[j]=metrics.metric_rri(data2)
    kurtis[j]=st.kurtosis(data2)
#%%

fig2=plt.figure()
# fig2.suptitle("Going Through a threshold", fontsize=10, fontweight='bold')
ax2 = fig2.add_subplot(111)
data=np.zeros_like(z)
mu_erf=0

#nice plot parameters: 
# s_2=0.8
# sig_n0=0.1

sig_n0=sigma_n0
s_2=s2
for i,d in enumerate(z):
    data[i]=expdef.noisytrans_erf(d,0,sigma_n0,s2)        
ax2.plot(z,data,'grey',alpha=0.7)
locs, labels = plt.yticks()  # Get the current locations and labels.
plt.yticks([4,8], ['$y_0$', '$y_0+2A_0$'],fontsize='large')  # Set label locations.
plt.xticks([])
plt.plot([mu_erf + 2*s_2, mu_erf + 2*s_2 ],[4,8],'--k')
plt.plot([mu_erf - 2*s_2,mu_erf - 2*s_2 ],[4,8],'--k')

plt.plot([3,8 ],[8-sig_n0,8-sig_n0],'--k')
plt.plot([3,8 ],[8+sig_n0,8+sig_n0],'--k')
plt.annotate('$\sigma_{n_0}$', [5,8-sig_n0-.4],fontsize='x-large')

xj=np.arange(-8,-5,0.01)
sigma_xj=0.3
mu_xj=-6.5
x_example=1/(sigma_xj*2*np.pi)*np.exp(-0.5*((xj-mu_xj)/sigma_xj)**2)+4
plt.plot(xj,x_example,'r',linewidth=2)
plt.plot([mu_xj,mu_xj],[3.5,1/(sigma_xj*2*np.pi)*np.exp(-0.5*((0)/sigma_xj)**2)+4],'--r')
plt.xticks([mu_xj,mu_erf - 2*s_2 ,mu_erf + 2*s_2],['$\mu$','$-s_2$','$s_2$'],fontsize='large')
plt.annotate('$x_j$',
             [mu_xj+1,1.05*((1/(sigma_xj*2*np.pi)*np.exp(-0.5*((0)/sigma_xj)**2)+4))],fontsize='x-large',
             xytext=(mu_xj-1, 1.05*((1/(sigma_xj*2*np.pi)*np.exp(-0.5*((0)/sigma_xj)**2)+4))) ,
             arrowprops=dict(arrowstyle="->"),va="center")

# plt.annotate("",
#             xy=(0.2, 0.2), xycoords='data',
#             xytext=(0.8, 0.8), textcoords='data',
#             arrowprops=dict(arrowstyle="->",
#                             connectionstyle="arc3"),
#             )
  # Set text labels.
# ax3 = ax2.twinx()  # instantiate a second axes that shares the same x-axis
# #
# #ax3.plot(mus,M,'r',label='Pareto  Normalized high')
# #ax3.plot(mus,M_low,'g',label='Pareto  Normalized low')
# ax3.plot(mus,M_mixed,'--k',label='Pareto  Normalized avg')
# ax3.plot(mus,M_max,'--m',label='Pareto  Normalized max')
# plt.legend()
# ax3.set_ylabel('Pareto Normalized')
# ax3.yaxis.label.set_color('red')
# ax3.tick_params(axis='y', colors='red')

method='knuth'#knuth
bin_num=50
plot_amnt=6
# mus=np.linspace(-range_vals,range_vals,25)
posit=range_vals*np.linspace(1,0,plot_amnt)**1.5
mus=-posit
posit.sort()
mus=np.append(mus,posit[1:])



'''set up  pcolor figures with std'''
# std=0.1
lims=0.4
amount_of_stats=len(mus) #○so it always includes 0
binss=np.linspace(4-8*sigma_n0,8+8*sigma_n0,bin_num+1)
avg_stat_bin=np.zeros((amount_of_stats))
std_stat_bin=np.zeros((amount_of_stats))
max_stat=np.zeros((amount_of_stats))

M=[]
bcount=[]
bval=[]

# tesmus=np.linspace(-testlim,testlim,15)
#tesmus=[-1,-0.5,0,0.5,1,20]
# r=0.1
fig2=plt.figure(figsize=(10,10))
# fig2.suptitle('Statistics aprox for r=(%.2f$\pm$%.2f), taken from %.1i shots with a std of %.2f. \n In red is shown the mean and the standar deviation.' %(r, r_std,num_points,std), fontsize=11, fontweight='bold')
ax = fig2.add_subplot(111, projection='3d')
avg_stat_bin=np.zeros_like(mus)
method='knuth'
bin_num=60
j=0
fig2.suptitle('Distributions along the transition.\n$\sigma/s_2$= %.2f ;\t $\sigma_{n_0}/A_0$= %.2f ' %(sigma/s2, sigma_n0/2),fontsize=16)

for j,mu in enumerate(mus):
    x=np.random.normal(mu, sigma,8000)
    data2=np.zeros_like(x)
    for i,d in enumerate(x):
      data2[i]=expdef.noisytrans_erf(d,0,sigma_n0,s2)    
    results=data2
    res_ct=astrost.histogram(results,bins=bin_num,density=True)
    # res_ct=astrost.histogram(results,bins=method)   
    num=res_ct[0]
    bcount.append(res_ct[0])
    bval.append(res_ct[1][:-1])
    M.append(mu*np.ones_like(res_ct[1][:-1]))
    avg_stat_bin[j]=np.mean(results)
    max_stat[j]=res_ct[1][np.argmax(num)]
    # ax.plot(res_ct[1][:-1],mean*np.ones([len(res_ct[1][:-1])]),res_ct[0],color='orange',alpha=0.5,linewidth=2)
    ax.plot(res_ct[1][:-1],mu*np.ones([len(res_ct[1][:-1])]),res_ct[0],color='blue',alpha=0.5,linewidth=1.5)
    # ax2.fill_betweenz(res_ct[1][:-1],mu*np.ones([len(res_ct[1][:-1])]),res_ct[0],color='red',alpha=0.25)
    #ax.add_collection3d(plt.fill_between(x,y,-0.1, color='orange', alpha=0.3,label="filled plot"),1, zdir='y')
    verts = [(res_ct[1][i],mu,res_ct[0][i]) for i in range(len(res_ct[0]))] + [(res_ct[1].max(),mu,0),(res_ct[1].min(),mu,0)]
    ax.add_collection3d(Poly3DCollection([verts],color='blue',alpha=0.1)) # Add a polygon instead of fill_between
    #    ax1.hist(initials,density=True,alpha=0.4,label='$<y_0>$='+str(mean))
ax.set_xlabel('$y$')
ax.set_ylabel('$\mu$')
ax.plot(4*np.ones_like(mus),mus,0,'--k',linewidth=3,alpha=0.9)
ax.plot(8*np.ones_like(mus),mus,0,'--k',linewidth=3,alpha=0.9)
ax.plot(avg_stat_bin,mus,0,'--r',linewidth=3,alpha=0.9,label='<y>')

# ax.plot(np.zeros_like(tesmus),tesmus,0,':k',alpha=0.8,linewidth=2)
ax2.set_xlim((4-8*sigma_n0,8+8*sigma_n0))

ax.legend()
# fig2.savefig(savefile+r'\line3d_dist.png')
plt.yticks([mu_erf - 2*s_2 ,mu_erf + 2*s_2],['$-s_2$','$s_2$'],fontsize='large')
plt.xticks([4,8 ],['$y_0$','$y_0+2A_0$'],fontsize='large')

ax.axes.zaxis.set_ticks([])

#%%

# '''set up  pcolor figures with std'''
# # std=0.1
# lims=0.4
# amount_of_stats=len(mus) #○so it always includes 0
# binss=np.linspace(4-8*sigma_n0,8+8*sigma_n0,bin_num+1)
# avg_stat_bin=np.zeros((amount_of_stats))
# std_stat_bin=np.zeros((amount_of_stats))
# max_stat=np.zeros((amount_of_stats))

# M=[]
# bcount=[]
# bval=[]

# # tesmus=np.linspace(-testlim,testlim,15)
# #tesmus=[-1,-0.5,0,0.5,1,20]
# # r=0.1
# fig2=plt.figure(figsize=(10,10))
# # fig2.suptitle('Statistics aprox for r=(%.2f$\pm$%.2f), taken from %.1i shots with a std of %.2f. \n In red is shown the mean and the standar deviation.' %(r, r_std,num_points,std), fontsize=11, fontweight='bold')
# ax = fig2.add_subplot(111, projection='3d')
# avg_stat_bin=np.zeros_like(mus)
# method='knuth'
# bin_num=60
# j=0
# fig2.suptitle('Distributions along the transition.\n$\sigma/s_2$= %.2f ;\t $\sigma_{n_0}/A_0$= %.2f ' %(sigma/s2, sigma_n0/2),fontsize=16)

# for j,mu in enumerate(mus):
#     x=np.random.normal(mu, sigma,8000)
#     data2=np.zeros_like(x)
#     for i,d in enumerate(x):
#       data2[i]=expdef.noisytrans_erf(d,0,sigma_n0,s2)    
#     results=data2
#     res_ct=astrost.histogram(results,bins=bin_num,density=True)
#     # res_ct=astrost.histogram(results,bins=method)   
#     num=res_ct[0]
#     bcount.append(res_ct[0])
#     bval.append(res_ct[1][:-1])
#     M.append(mu*np.ones_like(res_ct[1][:-1]))
#     avg_stat_bin[j]=np.mean(results)
#     max_stat[j]=res_ct[1][np.argmax(num)]
#     # ax.plot(res_ct[1][:-1],mean*np.ones([len(res_ct[1][:-1])]),res_ct[0],color='orange',alpha=0.5,linewidth=2)
#     ax.plot(res_ct[1][:-1],mu/sigma*np.ones([len(res_ct[1][:-1])]),res_ct[0],color='blue',alpha=0.5,linewidth=1.5)
#     # ax2.fill_betweenz(res_ct[1][:-1],mu*np.ones([len(res_ct[1][:-1])]),res_ct[0],color='red',alpha=0.25)
#     #ax.add_collection3d(plt.fill_between(x,y,-0.1, color='orange', alpha=0.3,label="filled plot"),1, zdir='y')
#     verts = [(res_ct[1][i],mu/sigma,res_ct[0][i]) for i in range(len(res_ct[0]))] + [(res_ct[1].max(),mu/sigma,0),(res_ct[1].min(),mu/sigma,0)]
#     ax.add_collection3d(Poly3DCollection([verts],color='blue',alpha=0.1)) # Add a polygon instead of fill_between
#     #    ax1.hist(initials,density=True,alpha=0.4,label='$<y_0>$='+str(mean))
# ax.set_xlabel('$y$')
# ax.set_ylabel('$\mu/\sigma$')
# ax.plot(4*np.ones_like(mus),mus/sigma,0,'--k',linewidth=3,alpha=0.9)
# ax.plot(8*np.ones_like(mus),mus/sigma,0,'--k',linewidth=3,alpha=0.9)
# ax.plot(avg_stat_bin,mus/sigma,0,'--r',linewidth=3,alpha=0.9,label='<y>')

# # ax.plot(np.zeros_like(tesmus),tesmus,0,':k',alpha=0.8,linewidth=2)
# ax2.set_xlim((4-8*sigma_n0,8+8*sigma_n0))

# ax.legend()
# # fig2.savefig(savefile+r'\line3d_dist.png')
# plt.yticks([mu_erf - 2*s_2 ,mu_erf + 2*s_2],['$-s_2/\sigma$','$s_2/\sigma$'],fontsize='large')
# plt.xticks([4,8 ],['$y_0$','$y_0+2A_0$'],fontsize='large')

# ax.axes.zaxis.set_ticks([])


