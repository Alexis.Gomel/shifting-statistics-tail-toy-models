# -*- coding: utf-8 -*-
"""
Created on Mon Aug 17 20:49:46 2020

@author: gomel
"""

import matplotlib.pyplot as plt
import matplotlib as mp
import numpy as np
import scipy.stats as st
import expdef
import metrics
import scipy.special as sp
import astropy.stats as astrost
import threshold
#import astropy.stats.knuth_bin_width as knuth
from scipy.interpolate import griddata
from mpl_toolkits.mplot3d import Axes3D


sigma_n0=0.06

def normi(x,sigma_n0,f):
    nor=np.zeros([len(x),len(f)])
    for j,nx in enumerate(nor):
        nor[j]=1/(sigma_n0*np.sqrt(2*np.pi))*np.exp(-((x[j]-f)**2)/((2*sigma_n0)**2))
    return nor

def f(y):
    offset=4
    A0=2
    j=0
    s2=1
    return A0*sp.erf((y-j)/(np.sqrt(2)*s2))+1+offset
    
xs=np.linspace(2,9,1500)
ys=np.linspace(-10,10,1500)

X, Y=np.meshgrid(xs, ys)
Z=normi(xs,sigma_n0,f(ys))

fig=plt.figure()
ax=fig.gca(projection='3d')
surf=ax.plot_surface(X,Y,Z)

#%%
fig=plt.figure()
surf=plt.pcolor(X,Y,Z)


#%%
fig=plt.figure()
surf=plt.plot(ys,Z[250,:])
