# -*- coding: utf-8 -*-

import scipy.stats as st
import numpy as np
import matplotlib.pyplot as plt
import scipy.special as sp
from matplotlib.animation import FuncAnimation,FFMpegFileWriter
#geofs idea jump transition
import threshold
import metrics as metrics 


def noisytrans(x,j,s):
  noiz = np.random.normal(0, s, 1)
  if x<=j: 
    return 4+noiz[0]
  if x>j:
    return 16+noiz[0]


z=np.arange(-10,10,0.001)

data=np.zeros_like(z)
for i,d in enumerate(z):
  data[i]=noisytrans(d,0,0.1)
plt.figure()
plt.plot(z,data)


sigma=0.5


fig2=plt.figure()
fig2.suptitle("Going Through a threshold.", fontsize=10, fontweight='bold')
mus=np.linspace(-1,1,5)

for j,mu in enumerate(mus):
    ax2 = fig2.add_subplot(len(mus),1,j+1)
    x=np.random.normal(mu, sigma, 1900)
    data2=np.zeros_like(x)
    for i,d in enumerate(x):
      data2[i]=noisytrans(d,0,0.1)    
    plt.hist(data2,bins=80,alpha=0.6,label='avg(x):'+str(mu))
    ax2.legend()
    if j+1<len(mus):
        ax2.axes.get_xaxis().set_visible(False)
    ax2.set_xlim((3,18))


#%%

fig4, ax = plt.subplots()
xdata, ydata = [], []

ln, = plt.plot([], [], 'r', animated=True)
f = np.linspace(-20, 20, 200)

z=f
data=np.zeros_like(z)
for i,d in enumerate(z):
  data[i]=noisytrans(d,0,0.03)
ax.plot(z,data)


def init():
	ax.set_xlim(-20, 20)
	ax.set_ylim(-0.9, 20)
	ln.set_data(xdata,ydata)
	return ln,


def update(frame):
	xdata.append(frame)
	ydata.append(noisytrans(frame,0,.3))
	ln.set_data(xdata, ydata)
	return ln,


ani = FuncAnimation(fig4, update, frames=f,
                    init_func=init, blit=True, interval = 2.5,repeat=False)
plt.show()	


#%%

mu_start=-10
mu_end=10
f = np.linspace(mu_start, mu_end, 30)
sigma=0.5
x=np.random.normal(f, sigma, 900)
ln, = plt.hist([], 'r', animated=True)

xdata, ydata = [], []

ln, = plt.plot([], [], 'r', animated=True)
f = np.linspace(-20, 20, 200)

z=f
data=np.zeros_like(z)
for i,d in enumerate(z):
  data[i]=noisytrans(d,0,0.03)
ax.plot(z,data)


def init():
	ax.set_xlim(-20, 20)
	ax.set_ylim(-0.9, 20)
	ln.set_data(xdata,ydata)
	return ln,



def update(frame):
	xdata.append(frame)
	ydata.append(noisytrans(frame,0,.3))
	ln.set_data(xdata, ydata)
	return ln,






ani = FuncAnimation(fig4, update, frames=f,
                    init_func=init, blit=True, interval = 2.5,repeat=False)
plt.show()	




def dat_hist(f,sigma):
    x=np.random.normal(f, sigma, 900)
    data=np.zeros_like(x)
    for i,d in enumerate(x):
        data[i]=noisytrans(d,0,3)
    return data

def update_hist(f,sigma, data):
    plt.cla()
    ln.set_data(dat_hist(f,sigma))
    return ln,        


def init():
	ax.set_xlim(-10, 10)
	ax.set_ylim(-0.25, 2)
	ax.hist(dat_hist(f,sigma),bins=30,alpha=0.6)
	return ln,


fig5, ax = plt.subplots()
ani = FuncAnimation(fig5, update_hist, frames=f,
                    init_func=init, blit=True, interval = 2.5,repeat=False)
plt.show()	    

#%%


fig4=plt.figure()
ax = fig4.add_subplot(2,1,1)
ax2 = fig4.add_subplot(2,1,2)

data = []
xdata, ydata = [], []

ln1, = plt.plot([], 'r', animated=True)
f = np.linspace(-20, 20, 200)

z=f
data=np.zeros_like(z)
for i,d in enumerate(z):
  data[i]=noisytrans(d,0,0.03)
ax.plot(z,data)


def init():
	ax.set_xlim(-20, 20)
	ax.set_ylim(-0.9, 20)
	ln1.set_data((xdata, 0),(xdata,ydata))
	return ln,



def update(frame):
	xdata=frame
	ydata=16.
	ln1.set_data((xdata, 0),(xdata,ydata))
	return ln,

#
#
#ln, = plt.hist([], animated=True)
#f = np.linspace(-20, 20, 200)
#
#z=f
#data=np.zeros_like(z)
#for i,d in enumerate(z):
#  data[i]=noisytrans(d,0,0.03)
#ax.plot(z,data)
#
#
#def init():
#	ax.set_xlim(-20, 20)
#	ax.set_ylim(-0.9, 20)
#	ln.set_data(data)
#	return ln,
#
#def update(frame):
#	data.append(noisytrans(frame,0,.3))
#	ln.set_data(data)
#	return ln,

ani = FuncAnimation(fig4, update, frames=f,
                    init_func=init, blit=True, interval = 2.5,repeat=False)
plt.show()	

#%%
#''' other stats''''
def noisytrans_erf(x,j,s,s2):
  noiz = np.random.normal(0, s, 1)
  return 4*(sp.erf((x-j)/(np.sqrt(2)*s2))+1)+4+noiz[0]

z=np.arange(-10,10,0.001)

data=np.zeros_like(z)
for i,d in enumerate(z):
  data[i]=noisytrans_erf(d,0,0.03,1)
plt.figure()
plt.plot(z,data)


sigma=0.5


fig2=plt.figure()
fig2.suptitle("Going Through a threshold.", fontsize=10, fontweight='bold')
mus=np.linspace(-3,3,7)

for j,mu in enumerate(mus):
    ax2 = fig2.add_subplot(len(mus),1,j+1)
    x=np.random.normal(mu, sigma, 1900)
    data2=np.zeros_like(x)
    for i,d in enumerate(x):
      data2[i]=noisytrans_erf(d,0,0.03,1)    
    plt.hist(data2,bins=30,alpha=0.6,label='avg(x)'+str(mu))
    ax2.legend()

    if j+1<len(mus):
        ax2.axes.get_xaxis().set_visible(False)
    ax2.set_xlim((3,14))

#%%
z=np.arange(-6,6,0.01)


fig2=plt.figure()
fig2.suptitle("Going Through a threshold - Metrics (5000 points).", fontsize=10, fontweight='bold')
ax2 = fig2.add_subplot(111)
data=np.zeros_like(z)
for i,d in enumerate(z):
  data[i]=noisytrans_erf(d,0,0.03,1)
ax2.plot(z,data)

k=0.2

sigma=0.5
mus=np.linspace(-6,6,70)
M=np.zeros_like(mus)
rri=np.zeros_like(mus)
kurtis=np.zeros_like(mus)

for j,mu in enumerate(mus):
#    ax2 = fig2.add_subplot(len(mus),1,j+1)
    x=np.random.normal(mu, sigma, 5000)
    data2=np.zeros_like(x)
    for i,d in enumerate(x):
      data2[i]=noisytrans_erf(d,0,0.03,1)    
    
    M[j]=metrics.metricm_norm(data2,k)
    rri[j]=metrics.metric_rri(data2)
    kurtis[j]=st.kurtosis(data2)
#    plt.hist(data2,bins=30,alpha=0.6,label='avg(x)'+str(mu))
#    ax2.legend()
ax3 = ax2.twinx()  # instantiate a second axes that shares the same x-axis
#
ax3.plot(mus,M,'r',label='Pareto  Normalized')
ax3.set_ylabel('Pareto Normalized')
ax3.yaxis.label.set_color('red')
ax3.tick_params(axis='y', colors='red')

fig2=plt.figure()
fig2.suptitle("Going Through a threshold - Metrics (5000 points).", fontsize=10, fontweight='bold')
ax2 = fig2.add_subplot(111)
ax2.plot(z,data)
ax3 = ax2.twinx()  # instantiate a second axes that shares the same x-axis
ax3.plot(mus,kurtis,'orange',label='kurtosis')
ax3.set_ylabel('Kurtosis')
ax3.yaxis.label.set_color('orange')
ax3.tick_params(axis='y', colors='orange')

fig2=plt.figure()
fig2.suptitle("Going Through a threshold - Metrics (5000 points).", fontsize=10, fontweight='bold')
ax2 = fig2.add_subplot(111)
ax2.plot(z,data)
ax3 = ax2.twinx()  # instantiate a second axes that shares the same x-axis
ax3.plot(mus,rri,'g',label='rri')
ax3.set_ylabel('RRI')


fig2=plt.figure()
fig2.suptitle("Going Through a threshold - Metrics (5000 points). Pareto vs Kurtosis", fontsize=10, fontweight='bold')
ax2 = fig2.add_subplot(111)
ax2.plot(mus,M,'r',label='Pareto  Normalized')
ax2.set_ylabel('Pareto Normalized')
ax2.yaxis.label.set_color('red')
ax2.tick_params(axis='y', colors='red')
ax3 = ax2.twinx()  # instantiate a second axes that shares the same x-axis
ax3.plot(mus,kurtis,'orange',label='kurtosis')
ax3.set_ylabel('Kurtosis')
ax3.yaxis.label.set_color('orange')
ax3.tick_params(axis='y', colors='orange')


#%%
def noisytrans_lin(x,j,s,m1,m2):
  noiz = np.random.normal(0, s, 1)
  if x<=j: 
    return m1*x +noiz[0]
  if x>j:
    return m2*x+j*(m1-m2)+noiz[0]


z=np.arange(-10,10,0.001)

data=np.zeros_like(z)
for i,d in enumerate(z):
  data[i]=noisytrans_lin(d,0,0.03,0.1,0.9)
plt.figure()
plt.plot(z,data)


sigma=0.5


fig2=plt.figure()
fig2.suptitle("Going Through a threshold.", fontsize=10, fontweight='bold')
mus=np.linspace(-2,3,10)

for j,mu in enumerate(mus):
    ax2 = fig2.add_subplot(len(mus),1,j+1)
    x=np.random.normal(mu, sigma, 4000)
    data2=np.zeros_like(x)
    for i,d in enumerate(x):
      data2[i]=noisytrans_lin(d,0,0.03,0.1,0.9)    
    plt.hist(data2,bins=30,alpha=0.6,label=str(mu))
    ax2.legend()
    if j+1<len(mus):
        ax2.axes.get_xaxis().set_visible(False)
    ax2.set_xlim((-2,6))



