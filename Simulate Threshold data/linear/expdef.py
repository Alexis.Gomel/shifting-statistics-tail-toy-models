import numpy as np
import scipy.special as sp


def noisytrans(x,j,s):
  noiz = np.random.normal(0, s, 1)
  if x<=j: 
    return 4+noiz[0]
  if x>j:
    return 8+noiz[0]

def noisytrans_erf(x,j,s,s2):
  """ offset gives an offset to the Erf function.  """
  offset=4
  noiz = np.random.normal(0, s, 1)
  A0=2
  return A0*(sp.erf((x-j)/(np.sqrt(2)*s2))+1)+offset+noiz[0]




def noisytrans_lin(x,j,s,m1,m2):
  """ Transition from measuring two linear functions  """

  noiz = np.random.normal(0, s, 1)
  if x<=j: 
    return m1*x +noiz[0]
  if x>j:
    return m2*x+j*(m1-m2)+noiz[0]


def noisytrans_cuad(x,s):
  """ Cuadratic function transition for the tail  """
  offset=0
  noiz = np.random.normal(0, s, 1)
  A0=1
  return A0*x**2+noiz[0]+offset

def noisytrans_sqrt(x,s):
  """ Cuadratic function transition for the tail  """
  offset=0
  noiz = np.random.normal(0, s, 1)
  A0=1
  return A0*np.sqrt(np.abs(x))+noiz[0]+offset
