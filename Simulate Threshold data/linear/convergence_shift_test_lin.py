# -*- coding: utf-8 -*-
"""
Created on Tue Oct  8 19:12:36 2019

@author: gomel
"""

import matplotlib.pyplot as plt
import matplotlib as mp
import numpy as np
import scipy.stats as st
import expdef
import metrics
import scipy.special as sp
import astropy.stats as astrost
import threshold
#import astropy.stats.knuth_bin_width as knuth
from scipy.interpolate import griddata
from mpl_toolkits.mplot3d import Axes3D 
import os 
import datetime as dt
from mpl_toolkits.mplot3d.art3d import Poly3DCollection # New import

now = dt.datetime.now()
# dd/mm/YY H:M:S
dt_string = now.strftime("%d_%m_%Y-%H_%M_%S")

#%%
sigma=.4
sigma_n0=0.06
# s2=1
m1=0.2
m2=0.9

range_vals=1+8*sigma

'''save parameters'''
now = dt.datetime.now()
dt_string = now.strftime("%d_%m_%Y-%H_%M_%S")
files=os.listdir()
savefile='.\\'+dt_string
os.makedirs(savefile)

'''print txt file with analysis parameters'''
analysis_txt=open(savefile+r"\analysis parameters.txt",'w+')
analysis_txt.write("Parameters for the analyzed data:"+'\n\n')
analysis_txt.write("sigma: %.2f+\n" %(sigma))
analysis_txt.write("sigma_n0: %.2f+\n" %(sigma_n0))
analysis_txt.write("m1: %.2f+\n" %(m1))
analysis_txt.write("m2: %.2f+\n" %(m2))

analysis_txt.close()

color_pareto=[0.141,0.118,0.306]     
color_kurtosis=[0.855,0.675,0]    
color_kr=[0.525,0.20,0.435]    
color_rri=[0.631,0.733,0.333]
color_kr3=[0.725, 0.2, 0.735]

# fig2=plt.figure()
# fig2.suptitle("Going Through a threshold.", fontsize=10, fontweight='bold')

# mus=np.linspace(-range_vals,range_vals,9)

# for j,mu in enumerate(mus):
#     ax2 = fig2.add_subplot(len(mus),1,j+1)
#     x=np.random.normal(mu, sigma, 5000)
#     data2=np.zeros_like(x)
#     for i,d in enumerate(x):
#       data2[i]=expdef.noisytrans_erf(d,0,sigma_n0,s2)    
#     height,bin_pos=astrost.histogram(data2,bins='blocks',density=True)
#     ax2.bar(bin_pos[:-1],height,width=np.diff(bin_pos),color='grey',align='edge',label='avg(x): '+str(mu))
# #    plt.hist(data2,bins=30,alpha=0.6,label='avg(x)'+str(mu))
#     ax2.legend()
#     if j+1<len(mus):
#         ax2.axes.get_xaxis().set_visible(False)
#     ax2.set_xlim((4-8*sigma_n0,8+8*sigma_n0))
#     ax2.set_yscale('log')
# fig2.savefig(savefile+r'\boxes_hist.png')
   
method='knuth'#knuth
bin_num=50
plot_amnt=8
# mus=np.linspace(-range_vals,range_vals,25)
posit=range_vals*np.linspace(1,0,plot_amnt)**1.4
mus=-posit
posit.sort()
mus=np.append(mus,posit[1:])

'''set up  pcolor figures with std'''
# std=0.1
lims=0.4
amount_of_stats=len(mus) #○so it always includes 0
binss=np.linspace(4-8*sigma_n0,8+8*sigma_n0,bin_num+1)
avg_stat_bin=np.zeros((amount_of_stats))
std_stat_bin=np.zeros((amount_of_stats))
max_stat=np.zeros((amount_of_stats))

M=[]
bcount=[]
bval=[]

# tesmus=np.linspace(-testlim,testlim,15)
#tesmus=[-1,-0.5,0,0.5,1,20]
# r=0.1
fig2=plt.figure(figsize=(13,10))
# fig2.suptitle('Statistics aprox for r=(%.2f$\pm$%.2f), taken from %.1i shots with a std of %.2f. \n In red is shown the mean and the standar deviation.' %(r, r_std,num_points,std), fontsize=11, fontweight='bold')
ax = fig2.add_subplot(111, projection='3d')
avg_stat_bin=np.zeros_like(mus)
method='knuth'
bin_num=60
j=0
fig2.suptitle('Distributions along the transition.\n$m1$= %.2f;$m2$= %.2f  ;\t $\sigma_{n_0}/A_0$= %.2f ' %(m1,m2, sigma_n0/2),fontsize=16)
for j,mu in enumerate(mus):
    x=np.random.normal(mu, sigma,7000)
    data2=np.zeros_like(x)
    for i,d in enumerate(x):
      data2[i]=expdef.noisytrans_lin(d,0,sigma_n0,m1,m2)   
    results=data2
    res_ct=astrost.histogram(results,bins=bin_num,density=True)
    # res_ct=astrost.histogram(results,bins=method)   
    num=res_ct[0]
    bcount.append(res_ct[0])
    bval.append(res_ct[1][:-1])
    M.append(mu*np.ones_like(res_ct[1][:-1]))
    avg_stat_bin[j]=np.mean(results)
    max_stat[j]=res_ct[1][np.argmax(num)]
    # ax.plot(res_ct[1][:-1],mean*np.ones([len(res_ct[1][:-1])]),res_ct[0],color='orange',alpha=0.5,linewidth=2)
    ax.plot(res_ct[1][:-1],mu*np.ones([len(res_ct[1][:-1])]),res_ct[0],color='blue',alpha=0.5,linewidth=1.5)
    # ax2.fill_betweenz(res_ct[1][:-1],mu*np.ones([len(res_ct[1][:-1])]),res_ct[0],color='red',alpha=0.25)
    #ax.add_collection3d(plt.fill_between(x,y,-0.1, color='orange', alpha=0.3,label="filled plot"),1, zdir='y')
    verts = [(res_ct[1][i],mu,res_ct[0][i]) for i in range(len(res_ct[0]))] + [(res_ct[1].max(),mu,0),(res_ct[1].min(),mu,0)]
    ax.add_collection3d(Poly3DCollection([verts],color='blue',alpha=0.1)) # Add a polygon instead of fill_between
    #    ax1.hist(initials,density=True,alpha=0.4,label='$<y_0>$='+str(mean))
ax.set_xlabel('$y$')
ax.set_ylabel('$\mu$')
# ax.plot(4*np.ones_like(mus),mus,0,'--k',linewidth=3,alpha=0.9)
# ax.plot(8*np.ones_like(mus),mus,0,'--k',linewidth=3,alpha=0.9)
ax.plot(avg_stat_bin,mus,0,'--r',linewidth=3,alpha=0.9,label='<y>')
# fig2.subplot_adjust(left=-0.11)
plt.tight_layout()
# ax.plot(np.zeros_like(tesmus),tesmus,0,':k',alpha=0.8,linewidth=2)
# ax.set_xlim((4-8*sigma_n0,8+8*sigma_n0))

ax.legend()
fig2.savefig(savefile+r'\line3d_dist.png')


#%%
method='knuth'#knuth
bin_num=50
mus=np.linspace(-range_vals,range_vals,60)

'''set up  pcolor figures with std'''
# std=0.1
lims=0.4
amount_of_stats=len(mus) #○so it always includes 0
binss=np.linspace(4-8*sigma_n0,8+8*sigma_n0,bin_num+1)
avg_stat_bin=np.zeros((amount_of_stats))
std_stat_bin=np.zeros((amount_of_stats))
max_stat=np.zeros((amount_of_stats))

M=[]
bcount=[]
bval=[]

xi = np.linspace(min(binss),max(binss),150)
yi = np.linspace(min(mus),max(mus),amount_of_stats)


for j,mu in enumerate(mus):
    x=np.random.normal(mu, sigma,7000)
    data2=np.zeros_like(x)
    for i,d in enumerate(x):
      data2[i]=expdef.noisytrans_lin(d,0,sigma_n0,m1,m2) 
    results=data2
    res_ct=astrost.histogram(results,bins=bin_num,density=True)
    # res_ct=astrost.histogram(results,bins=method)   
    num=res_ct[0]
    bcount.append(res_ct[0])
    bval.append(res_ct[1][:-1])
    M.append(mu*np.ones_like(res_ct[1][:-1]))
    avg_stat_bin[j]=np.mean(results)
    max_stat[j]=res_ct[1][np.argmax(num)]
    
    bval_flat=[item for sublist in bval for item in sublist]
    M_flat=[item for sublist in M for item in sublist]
    bcount_flat=[item for sublist in bcount for item in sublist]

zi = griddata((bval_flat,M_flat ),bcount_flat, (xi[None,:], yi[:,None]), method='cubic')
# zi[zi<0]=0; 

#%%
'''ugly 2d'''
# zi=zi/np.max(zi)
fig2=plt.figure(figsize=(10,10))
ax2=fig2.add_subplot(1,1,1)
# plt.contour(xi,yi,zi)
plt.pcolor(xi,yi,zi,cmap='jet')
# plt.imshow(xi, yi, zi)
# logzi=np.log(zi)
# plt.pcolor(xi,yi,logzi)
plt.plot(avg_stat_bin,yi,'--r',alpha=0.8,label='<y>')
plt.plot(max_stat,yi,'--k',alpha=0.8,label='peak of distribution')
ax2.set_ylabel('$\mu$')
ax2.set_xlabel('Transition')
plt.legend()
# plt.colormaps('JET')
plt.colorbar()    
plt.close(fig2)

#%%
'''nice 3d 1'''
fig2=plt.figure(figsize=(10,10))
# fig2.suptitle('Statistics scatter for r=(%.2f$\pm$%.2f), taken from %.1i shots with a std of %.2f. \n In red is shown the mean and the standar deviation.' %(r, r_std,num_points,std), fontsize=11, fontweight='bold')
ax2=fig2.add_subplot(1,1,1)
ax = fig2.add_subplot(111, projection='3d')
# ax.scatter(bval_flat,M_flat,bcount_flat/max(bcount_flat))
# ax.scatter(bval_flat,M_flat,(bcount_flat/max(bcount_flat)),alpha=0.3,norm=mp.colors.LogNorm())
z_vals=(bcount_flat/max(bcount_flat))
sc=ax.scatter(bval_flat,M_flat,z_vals,c=z_vals
              ,cmap='magma',alpha=0.8)
# ax.set_zlim([-6,0])
plt.colorbar(sc)
ax.set_xlabel('$\mu$')
ax.set_ylabel('Transition')
fig2.savefig(savefile+r'\3d_scater.png')
plt.close(fig2)

#%%
#i could interpolate this.
fig2=plt.figure(figsize=(10,10))
# fig2.suptitle('Statistics scatter for r=(%.2f$\pm$%.2f), taken from %.1i shots with a std of %.2f. \n In red is shown the mean and the standar deviation.' %(r, r_std,num_points,std), fontsize=11, fontweight='bold')
ax = fig2.add_subplot(111)
plt.title("2D histogram.\nMethod "+method
              , fontsize=10, fontweight='bold')
# ax.scatter(bval_flat,M_flat,bcount_flat/max(bcount_flat))
# ax.scatter(bval_flat,M_flat,(bcount_flat/max(bcount_flat)),alpha=0.3,norm=mp.colors.LogNorm())
z_vals=np.log(bcount_flat/max(bcount_flat))
sc=ax.scatter(bval_flat,M_flat,c=z_vals,edgecolors='none'
           ,marker='s',cmap='magma')
plt.plot(max_stat,yi,'--b',linewidth=3,alpha=0.8,label='peak of distribution')
plt.colorbar(sc)
plt.plot(avg_stat_bin,yi,color='blue',linewidth=4,alpha=0.8,label='Mean of distribution')
plt.gca().invert_yaxis()
ax.set_facecolor('grey')
ax.set_ylabel('$\mu$')
ax.set_xlabel('Transition')
plt.legend()
fig2.savefig(savefile+r'\2d_scater.png')
plt.close(fig2)
#%%
k=0.2
datalen=9000 #dataset len per step.

mus=np.linspace(-range_vals,range_vals,int(range_vals*20))
M=np.zeros_like(mus)
M_low=np.zeros_like(mus)

M_mixed=np.zeros_like(mus)
M_mixed05=np.zeros_like(mus)
M_mixed1=np.zeros_like(mus)
M_mixed3=np.zeros_like(mus)

M_max=np.zeros_like(mus)
M_max05=np.zeros_like(mus)
M_max1=np.zeros_like(mus)
M_max3=np.zeros_like(mus)
M_max5=np.zeros_like(mus)
M_c=np.zeros_like(mus,dtype=np.complex64)

rc=np.zeros_like(mus)
rri=np.zeros_like(mus)
kurtis=np.zeros_like(mus)
kr2=np.zeros_like(mus)
kr3=np.zeros_like(mus)
z=np.arange(-range_vals,range_vals,0.01)
var=np.zeros_like(mus)

for j,mu in enumerate(mus):
    x=np.random.normal(mu, sigma, datalen)
    data2=np.zeros_like(x)
    for i,d in enumerate(x):
      data2[i]=expdef.noisytrans_lin(d,0,sigma_n0,m1,m2)        
    M[j]=metrics.metricm_norm(data2,k)
    M_low[j]=metrics.metricm_norm_low(data2,k)
    
    M_mixed[j]=metrics.RTW_avg(data2,k)
    M_mixed05[j]=metrics.RTW_avg(data2,0.05)
    M_mixed3[j]=metrics.RTW_avg(data2,0.3)
    M_mixed1[j]=metrics.RTW_avg(data2,0.1) 
    
    M_max[j]=metrics.RTW_max(data2,k)
    M_max1[j]=metrics.RTW_max(data2,0.1)
    M_max3[j]=metrics.RTW_max(data2,0.3)
    M_max05[j]=metrics.RTW_max(data2,0.05)
    M_max5[j]=metrics.RTW_max(data2,0.5)
    M_c[j]=metrics.RTW_c(data2,0.5)
    rc[j]=metrics.metric_rc_tld(data2)
    rri[j]=metrics.metric_rri(data2)
    kurtis[j]=st.kurtosis(data2)
    kr2[j]=metrics.kr2(data2)
    kr3[j]=metrics.kr3(data2)
    var[j]=np.var(data2)
fig2=plt.figure()
fig2.suptitle("Going Through a threshold - Metrics (%.f points)." %(datalen), fontsize=10, fontweight='bold')
ax2 = fig2.add_subplot(111)
data=np.zeros_like(z)

for i,d in enumerate(z):
  data[i]=expdef.noisytrans_lin(d,0,sigma_n0,m1,m2) 
ax2.plot(z,data,'grey',alpha=0.7,
         label='$m1$= %.2f; $m2$= %.2f  \n$\sigma_{n_0}/A_0$= %.2f ' %(m1,m2, sigma_n0/2))
plt.legend(loc='upper left',bbox_to_anchor=(0,1))
ax3 = ax2.twinx()  # instantiate a second axes that shares the same x-axis
#
#ax3.plot(mus,M,'r',label='Pareto  Normalized high')
#ax3.plot(mus,M_low,'g',label='Pareto  Normalized low')
ax3.plot(mus,np.abs(M_c),'--k',label='Pareto complex')
ax3.plot(mus,M_max,color=color_pareto,label='Pareto  max')
plt.legend()
# ax3.set_ylabel('Pareto Normalized')
ax3.yaxis.label.set_color(color_pareto)
ax3.tick_params(axis='y', colors=color_pareto)
#%%
fig2=plt.figure()
fig2.suptitle('Distributions along the transition.\n$m1$= %.2f;$m2$= %.2f  ;\t $\sigma_{n_0}/A_0$= %.2f ' %(m1,m2, sigma_n0/2),fontsize=16)
ax2 = fig2.add_subplot(111)
data=np.zeros_like(z)
for i,d in enumerate(z):
  data[i]=expdef.noisytrans_lin(d,0,sigma_n0,m1,m2) 

ax2.plot(mus,np.abs(M_c),'k',label='|RTW_c|')
plt.legend(loc='upper right',bbox_to_anchor=(0.3,1))
ax3 = ax2.twinx()  # instantiate a second axes that shares the same x-axis
ax3.plot(mus,np.angle(M_c)/np.pi,'r',label='$\phi (RTW_c)$')
ax3.yaxis.set_major_formatter(mp.ticker.FormatStrFormatter('%g $\pi$'))
plt.legend(loc='upper right',bbox_to_anchor=(0.3,.9))# ax3.set_ylabel('Pareto Normalized')
ax3.yaxis.label.set_color('red')
ax3.tick_params(axis='y', colors='red')
ax3 = ax2.twinx()  # instantiate a second axes that shares the same x-axis
ax3.plot(z,data,'grey',alpha=0.7,
         label='$m1$= %.2f ;$m2$= %.2f  \n$\sigma_{n_0}/A_0$= %.2f ' %(m1,m2, sigma_n0/2))
ax3.set_yticks([])
fig2.savefig(savefile+r'\RTW_complex.png')

#%%
fig2=plt.figure()
fig2.suptitle("Going Through a threshold - Metrics (%.f points)." %(datalen), fontsize=10, fontweight='bold')
ax2 = fig2.add_subplot(111)
ax2.plot(z,data,'grey',alpha=0.7,
         label='$m1$= %.2f ;$m2$= %.2f  \n$\sigma_{n_0}/A_0$= %.2f ' %(m1,m2, sigma_n0/2))
plt.legend(loc='upper left',bbox_to_anchor=(0,1))
ax3 = ax2.twinx()  # instantiate a second axes that shares the same x-axis
#
#ax3.plot(mus,M,'r',label='Pareto  Normalized high')
#ax3.plot(mus,M_low,'g',label='Pareto  Normalized low')
ax3.plot(mus,var,'--k',label='variance')
ax3.set_yticks([])
ax3 = ax2.twinx()  # instantiate a second axes that shares the same x-axis
ax3.plot(mus,M_max,color=color_pareto,label='RTW')
ax3.set_yticks([])
ax3 = ax2.twinx()  # instantiate a second axes that shares the same x-axis
ax3.set_yticks([])
ax3.plot(mus,kr2,color=color_kr,label='Kr2')
ax3.set_yticks([])
plt.legend()
ax3.set_ylabel('')
ax3.yaxis.label.set_color(color_pareto)
ax3.tick_params(axis='y', colors=color_pareto)


#%%
fig2=plt.figure()
fig2.suptitle('Distributions along the transition.\n$m1$= %.2f;$m2$= %.2f  ;\t $\sigma_{n_0}/A_0$= %.2f ' %(m1,m2, sigma_n0/2),fontsize=16)
ax2 = fig2.add_subplot(111)
ax2.plot(z,data,'grey',alpha=0.7,
         label='$m1$= %.2f ;$m2$= %.2f  \n$\sigma_{n_0}/A_0$= %.2f ' %(m1,m2, sigma_n0/2))
ax2.set_ylabel('Transition')
plt.legend(loc='upper left',bbox_to_anchor=(0,1))
ax2.yaxis.label.set_color('grey')
ax2.tick_params(axis='y', colors='grey')
ax3 = ax2.twinx()  # instantiate a second axes that shares the same x-axis
ax3.set_yticks([])
ax3.plot(mus,M_max05,color=color_pareto,label='k=0.05',alpha=2*0.05)
ax3 = ax2.twinx()  # instantiate a second axes that shares the same x-axis
ax3.set_yticks([])
ax3.plot(mus,M_max1,color=color_pareto,label='k=0.1',alpha=2*0.1)
ax3 = ax2.twinx()  # instantiate a second axes that shares the same x-axis
ax3.plot(mus,M_max,color=color_pareto,linestyle='--',label='k=0.2',alpha=2*0.2)
ax3.set_yticks([])
ax3 = ax2.twinx()  # instantiate a second axes that shares the same x-axis
ax3.plot(mus,M_max3,color=color_pareto,label='k=0.3',alpha=2*0.3)
ax3.set_yticks([])
ax3 = ax2.twinx()  # instantiate a second axes that shares the same x-axis
ax3.plot(mus,M_max5,color=color_pareto,label='k=0.5',alpha=2*0.5)
ax3.set_yticks([])
plt.legend()
ax3.set_ylabel('Pareto metric')
ax3.yaxis.label.set_color('red')
ax3.tick_params(axis='y', colors='red')
# ax3.yaxis.label.set_color('red')

#%%
fig2=plt.figure(figsize=(13,10))
fig2.suptitle('Distributions along the transition.\n$m1$= %.2f;$m2$= %.2f  ;\t $\sigma_{n_0}/A_0$= %.2f ' %(m1,m2, sigma_n0/2),fontsize=16)
formatter = mp.ticker.ScalarFormatter(useMathText=True)
formatter.set_powerlimits((-1,1)) 
formatter.set_scientific(True) 
for j in range(6):
    ax3 = fig2.add_subplot(3,2,j+1)

    if j==0:
        ax3.plot(mus,kr2,color=color_kr,label='kr2',linewidth=1.5)
        ax3.yaxis.set_major_formatter(formatter) 
    # ax3.axes.yaxis.set_ticks([])
        # plt.legend()
        ax3.set_ylabel('Kr2',fontsize=13)
        # plt.legend(loc='upper right',bbox_to_anchor=(1,.8))
    if j==1:
        ax3.plot(mus,kr3,color=[0.725, 0.2, 0.735],label='kr3',linewidth=1.5)
        ax3.yaxis.set_major_formatter(formatter) 
        ax3.set_ylabel('Kr3',fontsize=13)
        # plt.legend()
        # plt.legend(loc='upper right',bbox_to_anchor=(1,.9))
        # ax3.axes.yaxis.set_ticks([])
    if j==2:
        ax3.plot(mus,M_max,color=color_pareto,label='RTW',linewidth=1.5)
        ax3.yaxis.set_major_formatter(formatter) 
    # ax3.axes.yaxis.set_ticks([])
        ax3.set_ylabel('RTW',fontsize=13)
        # plt.legend()
        # plt.legend(loc='upper right',bbox_to_anchor=(1,1))
    if j==3:
        ax3.plot(mus,rc,color=color_rri,label='rc',linewidth=1.5)
        ax3.yaxis.set_major_formatter(formatter) 
        # ax3.axes.yaxis.set_ticks([])
        # plt.legend()        
        ax3.set_ylabel('RR1',fontsize=13)
        # plt.legend(loc='upper right',bbox_to_anchor=(1,.7))
    if j==4:
        ax3.plot(mus,kurtis,color=color_kurtosis,label='kurtosis',linewidth=1.5)
        ax3.yaxis.set_major_formatter(formatter) 
        # ax3.axes.yaxis.set_ticks([])
        # plt.legend()
        ax3.set_ylabel('Kurtosis',fontsize=13)
# plt.legend(loc='upper right',bbox_to_anchor=(1,.6))
    if j==5:
        ax3.plot(mus,var,'r',label='variance',linewidth=1.5)
        ax3.yaxis.set_major_formatter(formatter) 
        # ax3.axes.yaxis.set_ticks([])
        # plt.legend(loc='upper right',bbox_to_anchor=(1,.6))
        # plt.legend()
        ax3.set_ylabel('Variance',fontsize=13)

    ax2 = ax3.twinx()  # instantiate a second axes that shares the same x-axi
    ax2.plot(z,data,'grey',alpha=0.7)
    if (j+1)%2!=0:   ax2.axes.yaxis.set_ticks([])
    # ax2.set_ylabel('Transition')
    ax2.yaxis.label.set_color('grey')
    ax3.set_xlabel('$\mu$')
    ax2.yaxis.label.set_color('red')
    # ax2.tick_params(axis='y', colors='red')
    ax2.tick_params(axis='y', colors='grey')
plt.tight_layout(rect=[0, 0.03, 1, 0.93])
fig2.savefig(savefile+r'\metricsal2.png')

#%%
# fig2=plt.figure()
# fig2.suptitle("Going Through a threshold - Metrics (5000 points).", fontsize=10, fontweight='bold')
# ax2 = fig2.add_subplot(111)
# ax2.plot(z,data,'grey',alpha=0.7)
# ax3 = ax2.twinx()  # instantiate a second axes that shares the same x-axis
# ax3.plot(mus,M_mixed05,label=' mixed k=0.05')
# ax3.plot(mus,M_mixed3,label=' mixed k=0.3')
# ax3.plot(mus,M_mixed1,label=' mixed k=0.1')
# ax3.plot(mus,M_mixed,'--k',label=' mixed k=0.2')
# plt.legend()
# ax3.set_ylabel('Pareto metric')
# ax3.yaxis.label.set_color('red')
# ax3.tick_params(axis='y', colors='red')


#%%
fig2=plt.figure()
fig2.suptitle("Going Through a threshold - Metrics (%.f points)." %(datalen), fontsize=10, fontweight='bold')
ax2 = fig2.add_subplot(111)
ax2.plot(z,data,'grey',alpha=0.7,
         label='$m1$= %.2f ;$m2$= %.2f  \n$\sigma_{n_0}/A_0$= %.2f ' %(m1,m2, sigma_n0/2))
plt.legend(loc='upper left',bbox_to_anchor=(0,1))
ax2.set_ylabel('Transition')
ax2.yaxis.label.set_color('grey')
ax2.tick_params(axis='y', colors='grey')
ax3 = ax2.twinx()  # instantiate a second axes that shares the same x-axis
ax3.plot(mus,kr2,color=color_kr,label='kr2')
# ax3 = ax2.twinx()  # instantiate a second axes that shares the same x-axis
ax3.plot(mus,kr3,color=[0.725, 0.2, 0.735],label='kr3')
ax3.set_ylabel('KR metrics')
ax3.yaxis.label.set_color('red')
ax3.tick_params(axis='y', colors='red')

#%%

fig2=plt.figure()
fig2.suptitle("Going Through a threshold - Metrics (%.f points)." %(datalen), fontsize=10, fontweight='bold')
ax2 = fig2.add_subplot(111)
ax2.plot(z,data,'grey',alpha=0.7,
         label='$m1$= %.2f ;$m2$= %.2f  \n$\sigma_{n_0}/A_0$= %.2f ' %(m1,m2, sigma_n0/2))
ax2.set_ylabel('Transition')
plt.legend(loc='upper left',bbox_to_anchor=(0,1))
ax2.yaxis.label.set_color('grey')
ax2.tick_params(axis='y', colors='grey')
ax2.set_xlabel('$\mu$')
ax3 = ax2.twinx()  # instantiate a second axes that shares the same x-axis
ax3.plot(mus,kr2,color=color_kr,label='kr2',linewidth=1.5)
ax3.axes.yaxis.set_ticks([])
plt.legend(loc='upper right',bbox_to_anchor=(1,.8))
ax3 = ax2.twinx() 
ax3.plot(mus,kr3,color=[0.725, 0.2, 0.735],label='kr3',linewidth=1.5)
plt.legend(loc='upper right',bbox_to_anchor=(1,.9))
ax3.axes.yaxis.set_ticks([])
ax3 = ax2.twinx() 
ax3.plot(mus,M_max,color=color_pareto,label='P2',linewidth=1.5)
ax3.axes.yaxis.set_ticks([])
plt.legend(loc='upper right',bbox_to_anchor=(1,1))
ax3 = ax2.twinx() 
ax3.plot(mus,rc,color=color_rri,label='rc',linewidth=1.5)
ax3.axes.yaxis.set_ticks([])
plt.legend(loc='upper right',bbox_to_anchor=(1,.7))
ax3 = ax2.twinx() 
ax3.plot(mus,kurtis,color=color_kurtosis,label='kurtosis',linewidth=1.5)
ax3.axes.yaxis.set_ticks([])
plt.legend(loc='upper right',bbox_to_anchor=(1,.6))
ax3.set_ylabel('metrics')
ax3.yaxis.label.set_color('red')
ax3.tick_params(axis='y', colors='red')

#%%

fig2=plt.figure()
fig2.suptitle("Going Through a threshold - Metrics (%.f points)." %(datalen), fontsize=10, fontweight='bold')
ax2 = fig2.add_subplot(111)
ax2.plot(z/sigma,data,'grey',alpha=0.7,
         label='$m1$= %.2f ;$m2$= %.2f  \n$\sigma_{n_0}/A_0$= %.2f ' %(m1,m2, sigma_n0/2))
plt.legend(loc='upper left',bbox_to_anchor=(0,1))
ax2.set_ylabel('Transition')
ax2.yaxis.label.set_color('grey')
ax2.set_xlabel('$\mu/\sigma$')
ax2.tick_params(axis='y', colors='grey')
ax3 = ax2.twinx()  # instantiate a second axes that shares the same x-axis
ax3.plot(mus/sigma,kr2,color=color_kr,label='kr2',linewidth=1.5)
ax3.axes.yaxis.set_ticks([])
plt.legend(loc='upper right',bbox_to_anchor=(1,.8))
ax3 = ax2.twinx() 
ax3.plot(mus/sigma,kr3,color=[0.725, 0.2, 0.735],label='kr3',linewidth=1.5)
plt.legend(loc='upper right',bbox_to_anchor=(1,.9))
ax3.axes.yaxis.set_ticks([])
ax3 = ax2.twinx() 
ax3.plot(mus/sigma,M_max,color=color_pareto,label='P2',linewidth=1.5)
ax3.axes.yaxis.set_ticks([])
plt.legend(loc='upper right',bbox_to_anchor=(1,1))
ax3 = ax2.twinx() 
ax3.plot(mus/sigma,rc,color=color_rri,label='rc',linewidth=1.5)
ax3.axes.yaxis.set_ticks([])
plt.legend(loc='upper right',bbox_to_anchor=(1,.7))
ax3 = ax2.twinx() 
ax3.plot(mus/sigma,kurtis,color=color_kurtosis,label='kurtosis',linewidth=1.5)
ax3.axes.yaxis.set_ticks([])
plt.legend(loc='upper right',bbox_to_anchor=(1,.6))
ax3.set_ylabel('metrics')
ax3.yaxis.label.set_color('red')
ax3.tick_params(axis='y', colors='red')


#%%



