# -*- coding: utf-8 -*-
"""
Created on Thu Jul 23 17:42:12 2020

Test changing the parameter of the P_max pareto metric
and the parameter of the KR metric. 

@author: gomel
"""

# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import numpy as np
import scipy.stats as st
import expdef
import metrics
import scipy.special as sp
import astropy.stats as astrost
import operator

range_vals=10
sigma=2.

def running_mean(x, N):
    cumsum = np.cumsum(np.insert(x, 0, 0)) 
    return (cumsum[N:] - cumsum[:-N]) / N

N_rmean=5


fig2=plt.figure()
fig2.suptitle("Going Through a threshold.", fontsize=10, fontweight='bold')
mus=np.linspace(-range_vals,range_vals,9)

for j,mu in enumerate(mus):
    ax2 = fig2.add_subplot(len(mus),1,j+1)
    x=np.random.normal(mu, sigma, 4000)
    data2=np.zeros_like(x)
    for i,d in enumerate(x):
      data2[i]=expdef.noisytrans_erf(d,0,0.03,1)    
    height,bin_pos=astrost.histogram(data2,bins='blocks',density=True)
    ax2.bar(bin_pos[:-1],height,width=np.diff(bin_pos),align='edge',label='avg(x): '+str(mu))
    ax2.legend()
    ax2.set_xlim((0,14))
    ax2.set_yscale('log')

    # height,bin_pos=astrost.histogram(x,bins='blocks',density=True)
    # ax2.bar(bin_pos[:-1],height,width=np.diff(bin_pos),align='edge',color='orange',alpha=0.3)
#    plt.hist(data2,bins=30,alpha=0.6,label='avg(x)'+str(mu))
    if j+1<len(mus):
        ax2.axes.get_xaxis().set_visible(False)
        ax2.axes.get_yaxis().set_visible(False)
    ax2.set_xlim((0,14))
    ax2.set_yscale('log')
#%%
kend=0.45
kstart=0.01
K_var=np.linspace(kstart,kend,30)
mus=np.linspace(-range_vals,range_vals,90)

M_start=np.zeros_like(mus)
M_end=np.zeros_like(mus)
M_2=np.zeros_like(mus)

M_temp=np.zeros_like(mus)

M_peaks=np.zeros_like(K_var)
M_peaks_pos=np.zeros_like(K_var)





kr_start=np.zeros_like(mus)
kr_end=np.zeros_like(mus)
kr_2=np.zeros_like(mus)
kr_3=np.zeros_like(mus)
kr_temp=np.zeros_like(mus)
kr_peaks=np.zeros_like(K_var)
kr_peaks_pos=np.zeros_like(K_var)

N_var=100*K_var
Nend=100*kend
Nstart=100*kstart

rc=np.zeros_like(mus)
rri=np.zeros_like(mus)
kurtis=np.zeros_like(mus)

z=np.arange(-range_vals,range_vals,0.01)

k2_index=min(enumerate(abs(K_var-0.2)),key=operator.itemgetter(1))[0]
k2=K_var[k2_index]


N2_index=min(enumerate(abs(N_var-5)),key=operator.itemgetter(1))[0]
N2=N_var[N2_index]
N3_index=min(enumerate(abs(K_var-0.02)),key=operator.itemgetter(1))[0]
N3=N_var[N3_index]


data_set=9000
#%%
for l,kvar_temp in enumerate(K_var) :

    for j,mu in enumerate(mus):
        x=np.random.normal(mu, sigma, 9000)
        data2=np.zeros_like(x)
        for i,d in enumerate(x):
          data2[i]=expdef.noisytrans_erf(d,0,0.03,1)        
    
        if kvar_temp==kstart:        
            M_start[j]=metrics.RTW_max(data2,kstart)
            kr_start[j]=metrics.krn(data2,100*kstart)
        if kvar_temp==kend:        
            M_end[j]=metrics.RTW_max(data2,kend)
            kr_end[j]=metrics.krn(data2,100*kend)
        if kvar_temp==k2:        
            M_2[j]=metrics.RTW_max(data2,k2)
            kr_3[j]=metrics.krn(data2,100*k2)
        if 100*kvar_temp==N2:    
            kr_2[j]=metrics.krn(data2,N2)


        M_temp[j]=metrics.RTW_max(data2,kvar_temp)
        kr_temp[j]=metrics.krn(data2,100*kvar_temp)

    print(l)
    index, M_peaks[l] = max(enumerate(M_temp[:int(len(mus)/2)]), key=operator.itemgetter(1))
    M_peaks_pos[l]=mus[index]
    index, kr_peaks[l] = max(enumerate(kr_temp[:int(len(mus)/2)]), key=operator.itemgetter(1))
    kr_peaks_pos[l]=mus[index]

#%%

fig2=plt.figure()
fig2.suptitle("Going Through a threshold (pareto) - (%.i) points."%(data_set), fontsize=10, fontweight='bold')
ax2 = fig2.add_subplot(121)
data=np.zeros_like(z)

for i,d in enumerate(z):
  data[i]=expdef.noisytrans_erf(d,0,0.03,1)
ax2.plot(z,data,'grey',alpha=0.7)

ax3 = ax2.twinx()  # instantiate a second axes that shares the same x-axis
#
#ax3.plot(mus,M,'r',label='Pareto  Normalized high')
#ax3.plot(mus,M_low,'g',label='Pareto  Normalized low')
ax3.plot(mus,M_start,'--b',label='k min=%.2f' %(kstart),alpha=0.5)
ax3.plot(mus,M_2,'--b',label='k=%.2f' %(k2),alpha=0.8)
ax3.plot(mus,M_end,'--b',label='k max=%.2f' %(kend))
ax3.plot(M_peaks_pos,M_peaks,'-ok',label='max')

plt.legend()
ax3.set_ylabel('Pareto vs k')
ax3.yaxis.label.set_color('blue')
ax3.tick_params(axis='y', colors='blue')
ax2.set_xlabel('$\mu$')


ax2 = fig2.add_subplot(122)
ax2.plot(M_peaks_pos,K_var,'-ok',label='max')
plt.ylabel('k')
plt.xlabel('max pos ($\mu$)')


#%%
fig2=plt.figure()
fig2.suptitle("Going Through a threshold (KR) - (%.i) points."%(data_set), fontsize=10, fontweight='bold')
ax2 = fig2.add_subplot(121)
data=np.zeros_like(z)

for i,d in enumerate(z):
  data[i]=expdef.noisytrans_erf(d,0,0.03,1)
ax2.plot(z,data,'grey',alpha=0.7)

ax3 = ax2.twinx()  # instantiate a second axes that shares the same x-axis
#
#ax3.plot(mus,M,'r',label='Pareto  Normalized high')
#ax3.plot(mus,M_low,'g',label='Pareto  Normalized low')
ax3.plot(mus,kr_start,'--b',label='N min=%.2f' %(Nstart),alpha=0.5)
ax3.plot(mus,kr_3,'--b',label='KR3',alpha=0.6)
ax3.plot(mus,kr_2,'--b',label='KR2',alpha=0.9)
ax3.plot(mus,kr_end,'--b',label='N max=%.2f' %(Nend),alpha=0.8)
ax3.plot(kr_peaks_pos,kr_peaks,'-ok',label='max')

plt.legend()
ax3.set_ylabel('KR vs N')
ax3.yaxis.label.set_color('blue')
ax3.tick_params(axis='y', colors='blue')
ax2.set_xlabel('$\mu$')


ax2 = fig2.add_subplot(122)
ax2.plot(kr_peaks_pos,N_var,'-ok',label='max')
plt.ylabel('N')
plt.xlabel('max pos ($\mu$)')

#%%
slope=np.diff(running_mean(M_start,N_rmean))/np.diff(mus)[1]
slope2=np.diff(running_mean(M_end,N_rmean))/np.diff(mus)[1]
slope_mus=mus[N_rmean:]-(mus[N_rmean]-mus[0])*0.5

fig_slope=plt.figure()
ax2 = fig_slope.add_subplot(211)
ax2.plot(slope_mus,slope)
ax2.plot([slope_mus[0],slope_mus[-1]],[0,0],'k')
ax3 = ax2.twinx()  # instantiate a second axes that shares the same x-axis
ax3.plot(mus,M_start,'grey',alpha=0.2)

ax2 = fig_slope.add_subplot(212)
ax2.plot(slope_mus,slope2)
ax2.plot([slope_mus[0],slope_mus[-1]],[0,0],'k')
ax3 = ax2.twinx()  # instantiate a second axes that shares the same x-axis
ax3.plot(mus,M_end,'grey',alpha=0.2)
ax2.set_xlabel('$\mu$')