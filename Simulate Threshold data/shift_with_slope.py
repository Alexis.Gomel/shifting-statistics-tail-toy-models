# -*- coding: utf-8 -*-
"""
Created on Tue Oct  8 17:24:00 2019

@author: gomel
"""


import matplotlib.pyplot as plt
import numpy as np
import scipy.stats as st
import expdef
import metrics
import scipy.special as sp
import astropy.stats as astrost

range_vals=12
sigma=2.
m1=0.2
m2=0.9
fig2=plt.figure()
fig2.suptitle("Going Through a threshold.", fontsize=10, fontweight='bold')
mus=np.linspace(-range_vals,range_vals,9)

for j,mu in enumerate(mus):
    ax2 = fig2.add_subplot(len(mus),1,j+1)
    x=np.random.normal(mu, sigma, 2900)
    data2=np.zeros_like(x)
    for i,d in enumerate(x):
      data2[i]=expdef.noisytrans_lin(d,0,0.03,m1,m2)    
    height,bin_pos=astrost.histogram(data2,bins='blocks',density=True)
    ax2.bar(bin_pos[:-1],height,width=np.diff(bin_pos),align='edge',label='avg(x): '+str(mu))
#    plt.hist(data2,bins=30,alpha=0.6,label='avg(x)'+str(mu))
    ax2.legend()
    if j+1<len(mus):
        ax2.axes.get_xaxis().set_visible(False)
    ax2.set_xlim((-8,15))
#    ax2.set_yscale('log')
#%%
k=0.2
mus=np.linspace(-range_vals,range_vals,70)
M=np.zeros_like(mus)
M_low=np.zeros_like(mus)
M_mixed=np.zeros_like(mus)
M_max=np.zeros_like(mus)
rc=np.zeros_like(mus)
rri=np.zeros_like(mus)
kurtis=np.zeros_like(mus)
z=np.arange(-range_vals,range_vals,0.01)
kr2=np.zeros_like(mus)
for j,mu in enumerate(mus):
    x=np.random.normal(mu, sigma, 9000)
    data2=np.zeros_like(x)
    for i,d in enumerate(x):
      data2[i]=expdef.noisytrans_lin(d,0,0.03,m1,m2)        
    M[j]=metrics.metricm_norm(data2,k)
    M_low[j]=metrics.metricm_norm_low(data2,k)
    M_mixed[j]=metrics.metricm_norm_mix(data2,k)
    M_max[j]=metrics.RTW_max(data2,k)
    rc[j]=metrics.metric_rc_tld(data2)
    rri[j]=metrics.metric_rri(data2)
    kurtis[j]=st.kurtosis(data2)
    kr2[j]=metrics.kr2(data2)

fig2=plt.figure()
fig2.suptitle("Going Through a threshold - Metrics (5000 points).", fontsize=10, fontweight='bold')
ax2 = fig2.add_subplot(111)
data=np.zeros_like(z)

for i,d in enumerate(z):
  data[i]=expdef.noisytrans_lin(d,0,0.03,m1,m2) 
ax2.plot(z,data,alpha=0.7)

ax3 = ax2.twinx()  # instantiate a second axes that shares the same x-axis
#
#ax3.plot(mus,M,'r',label='Pareto  Normalized high')
#ax3.plot(mus,M_low,'g',label='Pareto  Normalized low')
ax3.plot(mus,M_mixed,'--k',label='Pareto  Normalized avg')
ax3.plot(mus,M_max,'--m',label='Pareto  Normalized max')
plt.legend()
ax3.set_ylabel('Pareto Normalized')
ax3.yaxis.label.set_color('red')
ax3.tick_params(axis='y', colors='red')
#%%
fig2=plt.figure()
fig2.suptitle("Going Through a threshold - Metrics (5000 points).", fontsize=10, fontweight='bold')
ax2 = fig2.add_subplot(111)
ax2.plot(z,data,alpha=0.7)
ax3 = ax2.twinx()  # instantiate a second axes that shares the same x-axis
ax3.plot(mus,kurtis,'orange',label='kurtosis')
ax3.set_ylabel('Kurtosis')
ax3.yaxis.label.set_color('orange')
ax3.tick_params(axis='y', colors='orange')
plt.legend()
fig2=plt.figure()
fig2.suptitle("Going Through a threshold - Metrics (5000 points).", fontsize=10, fontweight='bold')
ax2 = fig2.add_subplot(111)
ax2.plot(z,data)
ax3 = ax2.twinx()  # instantiate a second axes that shares the same x-axis
ax3.plot(mus,rc/max(rc),'--c',label='rc')
ax3.plot(mus,rri/max(rri),'--m',label='rri')
ax3.set_ylabel('RRI')
plt.legend()

#%%
fig2=plt.figure()
fig2.suptitle("Going Through a threshold - Metrics (5000 points). Pareto vs Kurtosis", fontsize=10, fontweight='bold')
ax2 = fig2.add_subplot(111)
ax2.plot(mus,M,'r',label='Pareto  Normalized')
ax2.set_ylabel('Pareto Normalized')
ax2.yaxis.label.set_color('red')
ax2.tick_params(axis='y', colors='red')
ax3 = ax2.twinx()  # instantiate a second axes that shares the same x-axis
ax3.plot(mus,kurtis,'orange',label='kurtosis')
ax3.set_ylabel('Kurtosis')
ax3.yaxis.label.set_color('orange')
ax3.tick_params(axis='y', colors='orange')

#%%
fig2=plt.figure()
fig2.suptitle("Going Through a threshold - Metrics (5000 points). Pareto vs Kurtosis", fontsize=10, fontweight='bold')
ax2 = fig2.add_subplot(111)
ax2.plot(mus,M_max,'r',label='Pareto Max')
ax2.set_ylabel('Pareto Normalized')
ax2.yaxis.label.set_color('red')
ax2.tick_params(axis='y', colors='red')
#plt.legend()
ax3 = ax2.twinx()  # instantiate a second axes that shares the same x-axis
ax3.plot(mus,kurtis,'orange',label='kurtosis')
ax3.set_ylabel('Kurtosis')
ax3.yaxis.label.set_color('orange')
ax3.tick_params(axis='y', colors='orange')
plt.legend()
#%%
fig2=plt.figure()
fig2.suptitle("Going Through a threshold - Metrics (5000 points).", fontsize=10, fontweight='bold')
ax2 = fig2.add_subplot(111)
ax2.plot(z,data,'grey',alpha=0.7)
ax3 = ax2.twinx()  # instantiate a second axes that shares the same x-axis
ax3.plot(mus,(kr2-kr2[0])/max(kr2-kr2[0]),label='kr2')

#ax3.plot(mus,M,'r',label='Pareto  Normalized high')
ax3.plot(mus,kurtis/max(kurtis),color='orange',label='kurtosis')
ax3.plot(mus,rc/max(rc),'g',label='RRI')
ax3.plot(mus,(M_max-min(M_max))/max(M_max-min(M_max)),'m',label='Pareto  Normalized max')
plt.legend()
ax3.set_ylabel('Metrics Normalized')
#ax3.yaxis.label.set_color('red')
#ax3.tick_params(axis='y', colors='red')
#%%

fig2=plt.figure()
fig2.suptitle("Going Through a threshold - Metrics (5000 points). Pareto vs Kurtosis", fontsize=10, fontweight='bold')
ax2 = fig2.add_subplot(111)
ax2.plot(mus,rc,'g',label='RRI')
ax2.set_ylabel('RRI')
ax2.yaxis.label.set_color('green')
ax2.tick_params(axis='y', colors='green')
#plt.legend()
ax3 = ax2.twinx()  # instantiate a second axes that shares the same x-axis
ax3.plot(mus,kurtis,'orange',label='kurtosis')
ax3.set_ylabel('Kurtosis')
ax3.yaxis.label.set_color('orange')
ax3.tick_params(axis='y', colors='orange')
#pl
