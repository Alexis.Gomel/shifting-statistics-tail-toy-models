# -*- coding: utf-8 -*-
"""
Created on Mon Oct  7 14:05:31 2019

testing metrics on pithfork, with bootstrapped errors

@author: gomel
"""


import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint
import threshold
import metrics 
import astropy.stats as astrost
import boots

def f_p(y,t,r):
#    r=2.0
    return r*y-y**3

r=0.2
y0=1.2
xs = np.linspace(0,50,100)
    #    y0 = 10.0  # the initial condition
param=(r,)
ys = odeint(f_p, y0,xs,param)
ys = np.array(ys).flatten()

