# -*- coding: utf-8 -*-
"""
Created on Fri Aug 16 16:26:31 2019

@author: gomel
"""

'''
Pcolor plots with uneven data and interpolation
'''

import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint
#import threshold
#import metrics as metric
import astropy.stats as astrost
#import astropy.stats.knuth_bin_width as knuth
from scipy.interpolate import griddata
#from mpl_toolkits.mplot3d import Axes3D

def f_p(y,t,r):
    return r*y-y**3

def pitchfork_stats(r,r_std,t_fin,test_points,num_points):
    '''set variables to return'''        
    avg_stat_bin=np.zeros((len(test_points)))
    M=[]
    bcount=[]
    bval=[]
        
    '''binning'''
    method='knuth'
    bin_num=60

    for n,mean in enumerate(test_points):
        print(mean)
        results=[]
        for i in range(0,num_points):
            xs = np.linspace(0,t_fin,2)
            y0 = np.random.normal(mean,std,2)  # the initial condition
            param=(np.random.normal(r,r_std,1),)
            ys = odeint(f_p,y0[-1],xs,param) #integration
            results.append(ys[-1])
            
        res_ct=astrost.histogram(
                results,
                bins = bin_num ,
                density = True
                )#bins can be 'bin_num' or 'method'
        bcount.append(res_ct[0])
        bval.append(res_ct[1][:-1])
        M.append(mean*np.ones_like(res_ct[1][:-1]))
        avg_stat_bin[n]=np.median(results)       
        
    return bcount,bval,M,avg_stat_bin

############################################
'''parameters'''
std = 0.1 # stardard deviation of noise in the result.
r = 0.1 # mean value of r to simulate 
r_std = 0.03 # stardard deviation of noise in r
t_fin = 30 #integration time 

'''simulation parameters'''
num_points=3000 #values taken for each parameter

side = 15 #amount of initial values to test each side of 0
y0_lim = .50 #limits for the mean of the initial values
y0_points = np.linspace(-y0_lim,y0_lim,2*side+1) #mean initial values to simulate
y0_points = np.linspace(-y0_lim,y0_lim,2*side+1) #mean initial values to simulate


'''run simulation'''
bcount,bval,M,avg_stat_bin = pitchfork_stats(r,r_std,t_fin,y0_points,num_points)
    
#%%
'''plotting and analyzing'''
bcount_norm = [i/np.max(i) for i in bcount]
'''flatten array for plotting and gridding'''
bcount_norm_flat=[item for sublist in bcount_norm for item in sublist]
bval_flat=[item for sublist in bval for item in sublist]
M_flat=[item for sublist in M for item in sublist]
bcount_flat=[item for sublist in bcount for item in sublist]

''' set variables for smooth interpolation'''
xi = np.linspace(min(bval_flat),max(bval_flat),80)
yi = np.linspace(-y0_lim,y0_lim,40)
zi = griddata(
        (bval_flat,M_flat ),bcount_flat, (xi[None,:], yi[:,None]),\
        method='cubic'
        )


'''set pcolor type figure'''
fig2=plt.figure(figsize=(10,10))
fig2.suptitle(
    'Statistics interpolated pcolor for r=(%.2f$\pm$%.2f), taken from %.1i shots with a std of %.2f. \n In red is shown the mean and the standar deviation.'\
    %(r, r_std,num_points,std), fontsize=11, fontweight='bold'
        )
ax2=fig2.add_subplot(1,1,1)
#plt.pcolor(bval,M,bcount)
#plt.contourf(xi,yi,zi)
plt.pcolor(xi,yi,zi)
#plt.pcolor(xi,yi,np.log(zi))
plt.plot(avg_stat_bin,y0_points,'--r',alpha=0.8)
ax2.set_xlabel('$y*$ at $t=$%.1f' %(t_fin))
ax2.set_ylabel('$<y_0>$')
plt.colorbar()




#%%
''' 3d scatter plot'''
fig2=plt.figure(figsize=(16,9))
fig2.suptitle(
    'Statistics 3d scatter for r=(%.2f$\pm$%.2f), taken from %.1i shots with a std of %.2f. \n In red is shown the mean and the standar deviation.'\
    %(r, r_std,num_points,std), fontsize=11, fontweight='bold'
    )
ax = fig2.add_subplot(121, projection='3d')
ax.scatter(bval_flat,M_flat,bcount_flat)
ax.set_xlabel('$y*$ at $t=$%.1f' %(t_fin))
ax.set_ylabel('$<y_0>$')
ax.set_zlabel('Normalized to density function')

ax = fig2.add_subplot(122, projection='3d')
ax.scatter(bval_flat,M_flat,bcount_norm_flat)
ax.set_xlabel('$y*$ at $t=$%.1f' %(t_fin))
ax.set_ylabel('$<y_0>$')
ax.set_zlabel('Normalized to max of each run')
