# -*- coding: utf-8 -*-
"""
Created on Thu Aug 15 15:21:40 2019

Pitchfork Analysis metrics

@author: gomel
"""

import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint
import threshold
import metrics 
import astropy.stats as astrost
#import astropy.stats.knuth_bin_width as knuth
from scipy.interpolate import griddata
from mpl_toolkits.mplot3d import Axes3D
import datetime as dt
import os
import scipy.stats as st
from mpl_toolkits.mplot3d.art3d import Poly3DCollection # New import
import matplotlib as mp

def f_p(y,t,r):
    return r*y-y**3

'''parameters'''
r=.2
r_std=.05
lims=np.sqrt(r)
testlim=lims+np.sqrt(2*r_std)
t_fin=45
std=0.03


num_points=8000
amount_of_stats=160

'''Analysis parameters'''
k=0.2
'''extended'''
#set to 1 to have more plots. 
#set to 0 for only the plots that look good. 
extended=0 
'''save parameters'''
now = dt.datetime.now()
dt_string = now.strftime("%d_%m_%Y-%H_%M_%S")
files=os.listdir()
savefile='.\\'+dt_string
os.makedirs(savefile)

'''print txt file with analysis parameters'''
analysis_txt=open(savefile+r"\analysis parameters.txt",'w+')
analysis_txt.write("Parameters for the analyzed data:"+'\n\n')
analysis_txt.write("r: %.2f+\n" %(r))
analysis_txt.write("r_std: %.2f+\n" %(r_std))
analysis_txt.write("t0: %.2f+\n" %(t_fin))
analysis_txt.write("y0_std: %.2f+\n" %(std))
analysis_txt.close()

color_pareto=[0.141,0.118,0.306]     
color_kurtosis=[0.855,0.675,0]    
color_kr=[0.525,0.20,0.435]    
color_rri=[0.631,0.733,0.333]
color_kr3=[0.725, 0.2, 0.735]


#%%
method='knuth'
bin_num=60

'''set up  pcolor figures with std'''
#fig6=plt.figure(figsize=(10,10))
fig2=plt.figure(figsize=(10,10))
ax2=fig2.add_subplot(1,1,1)
#ax6=fig6.add_subplot(1,1,1)

binss=np.linspace(-(np.sqrt(r)+3*r_std),(np.sqrt(r)+3*r_std),bin_num+1)
pcolor_matrix=np.zeros((amount_of_stats,len(binss)-1))
pcolor_logmatrix=np.zeros((amount_of_stats,len(binss)-1))
avg_stat_bin=np.zeros((amount_of_stats))
std_stat_bin=np.zeros((amount_of_stats))
test_points=np.linspace(-testlim,testlim,amount_of_stats)

rc=np.zeros_like(test_points)
rri=np.zeros_like(test_points)
kurtis=np.zeros_like(test_points)
kr2=np.zeros_like(test_points)
kr3=np.zeros_like(test_points)
RTW=np.zeros_like(test_points)
var=np.zeros_like(test_points)
M_c=np.zeros_like(test_points,dtype=np.complex64)

xi = np.linspace(-(np.sqrt(r)+3*r_std),(np.sqrt(r)+3*r_std),80)
yi = np.linspace(-testlim,testlim,amount_of_stats)
# grid the data.
M=[]
val=[]
bcount=[]
bval=[]
    
fig2.suptitle('Statistics pcolor for r=(%.2f$\pm$%.2f), taken from %.1i shots with a std of %.2f. \n In red is shown the mean and the standar deviation.' %(r, r_std,num_points,std), fontsize=11, fontweight='bold')
for n,mean in enumerate(test_points):
    print(mean)
    initials=[]
    rs=[]
    results=[]

    for i in range(0,num_points):
        xs = np.linspace(0,t_fin,2)
        y0 = np.random.normal(mean,std,2)  # the initial condition
        param=(np.random.normal(r,r_std,1),)
        ys = odeint(f_p, y0[-1],xs,param)
        ys = np.array(ys).flatten()
        results.append(ys[-1])

    res_ct=astrost.histogram(results,bins=bin_num,density=True)
    res_ct2=astrost.histogram(results,bins=method,density=True)
    bcount.append(res_ct[0])
    bval.append(res_ct[1][:-1])
    M.append(mean*np.ones_like(res_ct[1][:-1]))
    avg_stat_bin[n]=np.mean(results)    
    rc[n]=metrics.metric_rc_tld(results)
    rri[n]=metrics.metric_rri(results)
    kurtis[n]=st.kurtosis(results)
    kr2[n]=metrics.kr2(results)
    kr3[n]=metrics.kr3(results)
    RTW[n]=metrics.RTW_max(results,k)
    M_c[n]=metrics.RTW_c(results)
    var[n]=np.var(results)
    num=res_ct[0]
#    num, bins, patches=ax6.hist(results, binss,alpha=0.9)
    pcolor_matrix[n,:]=num/np.max(num)
    pcolor_logmatrix[n,:]=np.log(num/np.max(num))
    avg_stat_bin[n]=np.mean(results)
    std_stat_bin[n]=np.std(results)
 
    
bval_flat=[item for sublist in bval for item in sublist]
M_flat=[item for sublist in M for item in sublist]
bcount_flat=[item for sublist in bcount for item in sublist]
x=res_ct[1][:-1]    
#zi = griddata((binss, test_points), pcolor_matrix, (xi[None,:], yi[:,None]), method='cubic')
# contour the gridded data, plotting dots at the randomly spaced data points.
ax2.set_xlabel('$y*$ at $t=$%.1f' %(t_fin))
ax2.set_ylabel('$<y_0>$')
cmap=ax2.pcolor(binss,test_points,pcolor_logmatrix)
fig2.colorbar(cmap)
fig2.savefig(savefile+r'\pcol_dist.png')
plt.close(fig2)
#ax2.plot(avg_stat_bin,test_points,'--r',alpha=0.6,label='mean')
#ax2.fill_betweenx(test_points,avg_stat_bin-std_stat_bin,avg_stat_bin+std_stat_bin,color='red',alpha=0.25)
#ax2.legend()
#%%
fig2=plt.figure()
fig2.suptitle("Metric for Pithfork transition. (%.i points).\n Integration time: %.2f, r= %.2f, $r_{std}$= %.2f " %(num_points,t_fin,r, r_std), fontsize=14)
ax2 = fig2.add_subplot(111)

ax2.plot(test_points,np.abs(M_c),'k',label='|RTW_c|')
plt.legend(loc='upper right',bbox_to_anchor=(0.3,1))
ax3 = ax2.twinx()  # instantiate a second axes that shares the same x-axis
ax3.plot(test_points,np.angle(M_c)/np.pi,'r',label='$\phi (RTW_c)$')
ax3.yaxis.set_major_formatter(mp.ticker.FormatStrFormatter('%g $\pi$'))
plt.legend(loc='upper right',bbox_to_anchor=(0.3,.9))# ax3.set_ylabel('Pareto Normalized')
ax3.yaxis.label.set_color('red')
ax3.tick_params(axis='y', colors='red')
ax3 = ax2.twinx()  # instantiate a second axes that shares the same x-axis
ax3.plot(test_points,avg_stat_bin,'grey',alpha=0.7)
ax3.set_yticks([])
ax2.set_xlabel('$<y_0>$')
fig2.savefig(savefile+r'\RTW_complex.png')



fig2=plt.figure()
ax2 = fig2.add_subplot(111)
points = np.array([test_points, np.abs(M_c)]).T.reshape(-1, 1, 2)
segments = np.concatenate([points[:-1], points[1:]], axis=1)
# Create the line collection object, setting the colormapping parameters.
# Have to set the actual values used for colormapping separately.
lc = LineCollection(segments, cmap=plt.get_cmap('twilight'))
lc.set_array(np.angle(M_c))
lc.set_linewidth(3)
plt.ylabel('|$RTW_c$|')
plt.gca().add_collection(lc)
plt.xlim(-range_vals,range_vals)
plt.ylim(np.min(np.abs(M_c)),np.max(np.abs(M_c)))
colb=plt.colorbar(lc)
colb.ax.yaxis.set_major_formatter(mp.ticker.FormatStrFormatter('%g $\pi$'))
ax3 = ax2.twinx()  # instantiate a second axes that shares the same x-axis
ax3.plot(test_points,avg_stat_bin,'grey',alpha=0.7)
ax3.set_yticks([])
ax3.yaxis.label.set_color('grey')
ax3.tick_params(axis='y', colors='grey')
# plt.show()
fig2.savefig(savefile+r'\RTW_complex_color.png')


#%%
'''jhbvlzfltzf'''
fig2=plt.figure(figsize=(10,10))
# fig2.suptitle('Statistics scatter for r=(%.2f$\pm$%.2f), taken from %.1i shots with a std of %.2f. \n In red is shown the mean and the standar deviation.' %(r, r_std,num_points,std), fontsize=11, fontweight='bold')
ax = fig2.add_subplot(111)
plt.title("2D histogram.\nMethod "+method
          , fontsize=10, fontweight='bold')
# ax.scatter(bval_flat,M_flat,bcount_flat/max(bcount_flat))
# ax.scatter(bval_flat,M_flat,(bcount_flat/max(bcount_flat)),alpha=0.3,norm=mp.colors.LogNorm())
z_vals=np.log(bcount_flat/max(bcount_flat))
# z_vals=(bcount_flat)
sc=ax.scatter(bval_flat,M_flat,c=z_vals,edgecolors='none'
       ,marker='s',s=110,cmap='magma')
# plt.plot(max_stat,yi,'--b',linewidth=3,alpha=0.8,label='peak of distribution')
plt.colorbar(sc)
# plt.plot(avg_stat_bin,yi,color='blue',linewidth=4,alpha=0.8,label='Mean of distribution')
plt.gca().invert_yaxis()
ax.set_facecolor('grey')
ax.set_ylabel('$<y_0>$')
ax.plot(avg_stat_bin,test_points,'--',color='blue',linewidth=1.5,alpha=0.9)
ax.plot(np.sqrt(r)*np.ones_like(test_points),test_points,'--',color='grey',linewidth=1.5,alpha=0.9)
ax.plot(-np.sqrt(r)*np.ones_like(test_points),test_points,'--',color='grey',linewidth=1.5,alpha=0.9)
ax.plot(np.zeros_like(test_points),test_points,'--',color='grey',linewidth=1,alpha=0.9)

ax.set_xlabel('$y^*$')
plt.legend()
fig2.savefig(savefile+r'\peak_density_2dhist.png')

#%%
# fig2=plt.figure()
# fig2.suptitle("Going Through a threshold - Metrics (5000 points).", fontsize=10, fontweight='bold')
# ax2 = fig2.add_subplot(111)
# ax2.plot(z,data,'grey',alpha=0.7)
# ax3 = ax2.twinx()  # instantiate a second axes that shares the same x-axis
# ax3.plot(mus,M_mixed05,label=' mixed k=0.05')
# ax3.plot(mus,M_mixed3,label=' mixed k=0.3')
# ax3.plot(mus,M_mixed1,label=' mixed k=0.1')
# ax3.plot(mus,M_mixed,'--k',label=' mixed k=0.2')
# plt.legend()
# ax3.set_ylabel('Pareto metric')
# ax3.yaxis.label.set_color('red')
# ax3.tick_params(axis='y', colors='red')

#%%
fig2=plt.figure()
# fig2.suptitle("Going Through a threshold - Metrics (%.f points)." %(datalen), fontsize=10, fontweight='bold')
ax2 = fig2.add_subplot(111)
# ax2.plot(z,data,'grey',alpha=0.7,
#          label='$\sigma/s_2$= %.2f \n$\sigma_{n_0}/A0$= %.2f ' %(sigma/s2, sigma_n0/2))
plt.legend(loc='upper left',bbox_to_anchor=(0,1))
ax2.set_ylabel('Transition')
ax2.yaxis.label.set_color('grey')
ax2.tick_params(axis='y', colors='grey')
# ax3 = ax2.twinx()  # instantiate a second axes that shares the same x-axis
ax2.plot(test_points,kr2,color=color_kr,label='kr2')
# ax3 = ax2.twinx()  # instantiate a second axes that shares the same x-axis
ax2.plot(test_points,kr3,color=[0.725, 0.2, 0.735],label='kr3')
ax2.set_ylabel('KR metrics')
ax2.yaxis.label.set_color('red')
ax2.tick_params(axis='y', colors='red')
fig2.savefig(savefile+r'\metricskr.png')

#%%
fig2=plt.figure()
fig2.suptitle("Going Through a threshold - Metrics (%.f points).\n Integration time: %.2f" %(num_points,t_fin), fontsize=10, fontweight='bold')
ax2 = fig2.add_subplot(111)
ax2.plot(test_points, avg_stat_bin,'grey',alpha=0.7,linewidth=2.5,
         label='$<\!y^*\!>$. r= %.2f \n$r_std$= %.2f ' %(r, r_std))
plt.legend(loc='upper left',bbox_to_anchor=(0,1))
ax2.set_ylabel('Transition')
ax2.yaxis.label.set_color('grey')
ax2.set_xlabel('$<\!y_0\!>$')
ax2.tick_params(axis='y', colors='grey')
ax3 = ax2.twinx()  # instantiate a second axes that shares the same x-axis
ax3.plot(test_points,kr2,color=color_kr,label='kr2',linewidth=1.5)
ax3.axes.yaxis.set_ticks([])
plt.legend(loc='upper right',bbox_to_anchor=(1,.8))
ax3 = ax2.twinx() 
ax3.plot(test_points,kr3,color=[0.725, 0.2, 0.735],label='kr3',linewidth=1.5)
plt.legend(loc='upper right',bbox_to_anchor=(1,.9))
ax3.axes.yaxis.set_ticks([])
ax3 = ax2.twinx() 
ax3.plot(test_points,RTW,color=color_pareto,label='P2',linewidth=1.5)
ax3.axes.yaxis.set_ticks([])
plt.legend(loc='upper right',bbox_to_anchor=(1,1))
ax3 = ax2.twinx() 
ax3.plot(test_points,rc,color=color_rri,label='rc',linewidth=1.5)
ax3.axes.yaxis.set_ticks([])
plt.legend(loc='upper right',bbox_to_anchor=(1,.7))
ax3 = ax2.twinx() 
ax3.plot(test_points,kurtis,color=color_kurtosis,label='kurtosis',linewidth=1.5)
ax3.axes.yaxis.set_ticks([])
plt.legend(loc='upper right',bbox_to_anchor=(1,.6))
ax3.set_ylabel('metrics')
ax3.yaxis.label.set_color('red')
ax3.tick_params(axis='y', colors='red')
fig2.savefig(savefile+r'\metricsall.png')
#%%

#%%

fig2=plt.figure(figsize=(13,10))
fig2.suptitle("Metric for Pithfork transition. (%.i points).\n Integration time: %.2f, r= %.2f, $r_{std}$= %.2f " %(num_points,t_fin,r, r_std), fontsize=14)
formatter = mp.ticker.ScalarFormatter(useMathText=True)
formatter.set_powerlimits((-1,1)) 
formatter.set_scientific(True) 
for j in range(6):
    ax3 = fig2.add_subplot(3,2,j+1)
    if j==0:
        ax3.plot(test_points,kr2,color=color_kr,label='Kr2',linewidth=1.5)
        ax3.yaxis.set_major_formatter(formatter) 
        ax3.set_ylabel('Kr2',fontsize=13)
        # plt.legend(loc='upper right',bbox_to_anchor=(1,.8))
    if j==1:
        ax3.plot(test_points,kr3,color=[0.725, 0.2, 0.735],label='Kr3',linewidth=1.5)
        ax3.yaxis.set_major_formatter(formatter) 
        ax3.set_ylabel('Kr3',fontsize=13)
  # plt.legend(loc='upper right',bbox_to_anchor=(1,.9))
        # ax3.axes.yaxis.set_ticks([])
    if j==2:
        ax3.plot(test_points,RTW,color=color_pareto,label='RTW',linewidth=1.5)
        ax3.yaxis.set_major_formatter(formatter) 
        ax3.set_ylabel('RTW',fontsize=13)
   # ax3.axes.yaxis.set_ticks([])
        # plt.legend(loc='upper right',bbox_to_anchor=(1,1))
    if j==3:
        ax3.plot(test_points,rc,color=color_rri,label='rc',linewidth=1.5)
        ax3.yaxis.set_major_formatter(formatter) 
        ax3.set_ylabel('rri',fontsize=13)
       # ax3.axes.yaxis.set_ticks([])
        # plt.legend(loc='upper right',bbox_to_anchor=(1,.7))
    if j==4:
        ax3.plot(test_points,kurtis,color=color_kurtosis,label='kurtosis',linewidth=1.5)
        ax3.yaxis.set_major_formatter(formatter) 
        ax3.set_ylabel('Kurtosis',fontsize=13)
    if j==5:
        ax3.plot(test_points,var,'r',label='variance',linewidth=1.5)
        ax3.yaxis.set_major_formatter(formatter) 
        ax3.set_ylabel('Variance',fontsize=13)
     # ax3.axes.yaxis.set_ticks([])
        # plt.legend(loc='upper right',bbox_to_anchor=(1,.6))        
    ax3.yaxis.set_major_formatter(formatter) 
    ax2 = ax3.twinx()  # instantiate a second axes that shares the same x-axi
    ax2.plot(test_points, avg_stat_bin,'grey',alpha=0.7,linewidth=2.5,
                 label='$<\!y^*\!>$')
    if (j+1)%2!=0:   ax2.axes.yaxis.set_ticks([])
    ax2.yaxis.label.set_color('grey')
    if j==4:   ax3.set_xlabel('$<\!y_0\!>$')
    if j==5:   ax3.set_xlabel('$<\!y_0\!>$')
    ax2.tick_params(axis='y', colors='grey')
    ax2.plot(test_points,np.sqrt(r)*np.ones_like(test_points),'--',color='grey',linewidth=1,alpha=0.5)
    ax2.plot(test_points,-np.sqrt(r)*np.ones_like(test_points),'--',color='grey',linewidth=1,alpha=0.5)
    ax2.plot(test_points,np.zeros_like(test_points),'--',color='grey',linewidth=1,alpha=0.5)
plt.tight_layout(rect=[0, 0.03, 1, 0.93])
# plt.legend()
fig2.savefig(savefile+r'\metricsal2.png')

#%%
'''
Pcolor plots with uneven data and interpolation
'''
method='knuth'
bin_num=60

'''set up  pcolor figures with std'''

side=6
amount_of_stats=2*side+1 #○so it always includes 0
binss=np.linspace(-0.6,0.6,bin_num+1)
avg_stat_bin=np.zeros((amount_of_stats))
std_stat_bin=np.zeros((amount_of_stats))
test_points=np.linspace(-testlim,testlim,amount_of_stats)
j=0

M=[]
bcount=[]
bval=[]

for n,mean in enumerate(test_points):
    j=j+1 
    print(mean)
    initials=[]
    rs=[]
    results=[]
    for i in range(0,num_points):
        xs = np.linspace(0,t_fin,2)
        y0 = np.random.normal(mean,std,2)  # the initial condition
        param=(np.random.normal(r,r_std,1),)
        ys = odeint(f_p, y0[-1],xs,param)
        results.append(ys[-1])
        initials.append(y0[-1])

    res_ct=astrost.histogram(results,bins=method,density=True)
    bcount.append(res_ct[0])
    bval.append(res_ct[1][:-1])
    M.append(mean*np.ones_like(res_ct[1][:-1]))
    avg_stat_bin[n]=np.mean(results)
#    res_ct2=astrost.histogram(results,bins=method,density=True)
    
#    num=res_ct[0]
##    num, bins, patches=ax6.hist(results, binss,alpha=0.9)
#    pcolor_matrix[n,:]=num/np.max(num)
#    pcolor_logmatrix[n,:]=np.log(num/np.max(num))
#    avg_stat_bin[n]=np.mean(results)
#    std_stat_bin[n]=np.std(results)

bval_flat=[item for sublist in bval for item in sublist]
M_flat=[item for sublist in M for item in sublist]
bcount_flat=[item for sublist in bcount for item in sublist]


xi = np.linspace(min(binss),max(binss),150)
yi = np.linspace(-testlim,testlim,150)
zi = griddata((bval_flat,M_flat),bcount_flat, (xi[None,:], yi[:,None]), method='cubic')

#%%
fig2=plt.figure(figsize=(10,10))
fig2.suptitle('Statistics scatter for r=(%.2f$\pm$%.2f), taken from %.1i shots with a std of %.2f. \n In red is shown the mean and the standar deviation.' %(r, r_std,num_points,std), fontsize=11, fontweight='bold')
ax = fig2.add_subplot(111, projection='3d')
ax.scatter(bval_flat,M_flat,bcount_flat)
ax.set_xlabel('$y*$ at $t=$%.1f' %(t_fin))
ax.set_ylabel('$<y_0>$')

fig2=plt.figure(figsize=(10,10))
fig2.suptitle('Statistics scatter for r=(%.2f$\pm$%.2f), taken from %.1i shots with a std of %.2f. \n In red is shown the mean and the standar deviation.' %(r, r_std,num_points,std), fontsize=11, fontweight='bold')
ax2=fig2.add_subplot(1,1,1)
#plt.contourf(bval,M,bcount)
#plt.contour(xi,yi,zi)
plt.pcolor(xi,yi,zi)
#plt.pcolor(xi,yi,np.log(zi))
plt.plot(avg_stat_bin,test_points,'--r',alpha=0.8)
ax2.plot(np.sqrt(r)*np.ones_like(test_points),test_points,'--',color='grey',linewidth=1.5,alpha=0.9)
ax2.plot(-np.sqrt(r)*np.ones_like(test_points),test_points,'--',color='grey',linewidth=1.5,alpha=0.9)
ax2.plot(np.zeros_like(test_points),test_points,'--',color='grey',linewidth=1,alpha=0.9)

ax2.set_xlabel('$y*$ at $t=$%.1f' %(t_fin))
ax2.set_ylabel('$<y_0>$')
plt.colorbar()
fig2.savefig(savefile+r'\pcolor_dist.png')
# plt.close(fig2)

#%%
test_points=np.linspace(-testlim,testlim,11)
#test_points=[-1,-0.5,0,0.5,1,20]
# r=0.1
fig2=plt.figure(figsize=(13,10))
fig2.suptitle('Distributions along the Pitchfork transition for r=(%.2f$\pm$%.2f) and $t^*$=%.2f.\n taken from %.1i shots with a std of %.2f. ' %(r, r_std,t_fin,num_points,std), fontsize=16)
ax = fig2.add_subplot(111, projection='3d')
avg_stat_bin=np.zeros_like(test_points)
method='knuth'
bin_num=60
j=0
for mean in test_points:
    initials=[]
    rs=[]
    results=[]
    for i in range(0,num_points):
        xs = np.linspace(0,t_fin,2)
        y0 = np.random.normal(mean,std,2)  # the initial condition
        param=(np.random.normal(r,r_std,1),)
        ys = odeint(f_p, y0[-1],xs,param)
        ys = np.array(ys).flatten()
        results.append(ys[-1])
        initials.append(y0[-1])
    avg_stat_bin[j]=np.mean(results)
    j+=1
    # res_ct=astrost.histogram(results,bins=bin_num,density=True)
    res_ct2=astrost.histogram(results,bins=method,density=True)
    # ax.plot(res_ct[1][:-1],mean*np.ones([len(res_ct[1][:-1])]),res_ct[0],color='orange',alpha=0.5,linewidth=2)
    ax.plot(res_ct2[1][:-1],mean*np.ones([len(res_ct2[1][:-1])]),res_ct2[0],color='blue',alpha=0.5,linewidth=1.5)
    verts = [(res_ct2[1][i],mean,res_ct2[0][i]) for i in range(len(res_ct2[0]))] + [(res_ct2[1].max(),mean,0),(res_ct2[1].min(),mean,0)]
    ax.add_collection3d(Poly3DCollection([verts],color='blue',alpha=0.1)) # Add a polygon instead of fill_between
    #    ax1.hist(initials,density=True,alpha=0.4,label='$<y_0>$='+str(mean))
plt.tight_layout()
ax.set_xlim((-(2*r_std+lims),(2*r_std+lims)))
ax.set_xlabel('$y*$')
ax.set_ylabel('$<y_0>$')
ax.plot(np.sqrt(r)*np.ones_like(test_points),test_points,0,'--k',linewidth=3,alpha=0.9)
ax.plot(-np.sqrt(r)*np.ones_like(test_points),test_points,0,'--k',linewidth=3,alpha=0.9)
ax.plot(np.zeros_like(test_points),test_points,0,'--k',linewidth=3,alpha=0.9)
ax.plot(avg_stat_bin,test_points,0,'--r',linewidth=3,alpha=0.9,label='<y*>')
# ax.plot(test_points,test_points,0,'--',color='orange',alpha=0.9,label='<y*>')
ax.legend()
fig2.savefig(savefile+r'\line3d_dist.png')
# plt.close(fig2)

#%%
if extended==1:
    '''
    hexbon and 2dhist plots
    '''
    
    method='knuth'
    bin_num=60
    
    side=10
    amount_of_stats=2*side+1 #○so it always includes 0
    binss=np.linspace(-0.6,0.6,bin_num+1)
    
    pcolor_matrix=np.zeros((amount_of_stats,len(binss)-1))
    pcolor_logmatrix=np.zeros((amount_of_stats,len(binss)-1))
    
    avg_stat_bin=np.zeros((amount_of_stats))
    std_stat_bin=np.zeros((amount_of_stats))
    test_points=np.linspace(-testlim,testlim,amount_of_stats)
    
    j=0
    
    M=[]
    val=[]
    bcount=[]
    bval=[]
    
    for n,mean in enumerate(test_points):
        j=j+1 
        print(mean)
        initials=[]
        rs=[]
        results=[]
        for i in range(0,num_points):
            xs = np.linspace(0,t_fin,2)
            y0 = np.random.normal(mean,std,2)  # the initial condition
            param=(np.random.normal(r,r_std,1),)
            ys = odeint(f_p, y0[-1],xs,param)
            M.append(ys[-1])
            val.append(mean)
    
    fig2=plt.figure(figsize=(10,10))
    fig2.suptitle('Statistics scatter for r=(%.2f$\pm$%.2f), taken from %.1i shots with a std of %.2f. \n In red is shown the mean and the standar deviation.' %(r, r_std,num_points,std), fontsize=11, fontweight='bold')
    ax2=fig2.add_subplot(1,1,1)
    plt.hist2d(np.array(M).flatten(),np.array(val).flatten(),bins=(40,amount_of_stats),density='True')
    ax2.set_xlabel('$y*$ at $t=$%.1f' %(t_fin))
    ax2.set_ylabel('$<y_0>$')
    fig2.savefig(savefile+r'\hist2d_dist.png')
    
    fig2=plt.figure(figsize=(10,10))
    fig2.suptitle('Statistics hexbin for r=(%.2f$\pm$%.2f), taken from %.1i shots with a std of %.2f. \n In red is shown the mean and the standar deviation.' %(r, r_std,num_points,std), fontsize=11, fontweight='bold')
    ax2=fig2.add_subplot(1,1,1)
    plt.hexbin(np.array(M).flatten(),np.array(val).flatten(),gridsize=15)
    ax2.set_xlabel('$y*$ at $t=$%.1f' %(t_fin))
    ax2.set_ylabel('$<y_0>$')
    #plt.pcolormesh(xi, yi, zi.reshape(xi.shape)pcolormesh(xi, yi, zi.reshape(xi.shape)
    fig2.savefig(savefile+r'\hexbin_dist.png')
    plt.close(fig2)
    
    
    '''widonwed aprox smooth distributions'''
    
    amount_of_stats=7
    test_points=np.linspace(-testlim,testlim,7)
    #test_points=[-1,-0.5,0,0.5,1,20]
    # r=0.1
    fig2=plt.figure()
    ax1=fig2.add_subplot(len(test_points),1,1)
    j=0
    method='knuth'
    bin_num=60
    fig2.suptitle('Comparison between method'+method+'\n and constant bining with %.1i bins from %.1i points'%(bin_num,num_points), fontsize=11, fontweight='bold')
    
    for mean in test_points:
        initials=[]
        rs=[]
        results=[]
        for i in range(0,num_points):
            xs = np.linspace(0,t_fin,2)
            y0 = np.random.normal(mean,std,2)  # the initial condition
            param=(np.random.normal(r,r_std,1),)
            ys = odeint(f_p, y0[-1],xs,param)
            ys = np.array(ys).flatten()
            results.append(ys[-1])
            initials.append(y0[-1])
        j+=1     
        ax1=fig2.add_subplot(len(test_points),1,j)
        if j<7:
            ax1.axes.get_xaxis().set_visible(False)
            res_ct=astrost.histogram(results,bins=bin_num,density=True)
            res_ct2=astrost.histogram(results,bins=method,density=True)
        ax1.plot(res_ct[1][:-1],res_ct[0])
        ax1.plot(res_ct2[1][:-1],res_ct2[0])
        #    ax1.hist(initials,density=True,alpha=0.4,label='$<y_0>$='+str(mean))
        ax1.set_xlim((-1.3*lims,1.3*lims))
    #    ax1.set_ylim((0,10))
    #    ax1.set_yscale('log')
    ax1.legend()
    ax1.set_xlabel('$y*$')
    fig2.savefig(savefile+r'\line2d_dist.png')
    plt.close(fig2)
#%%
#fig2=plt.figure(figsize=(10,10))
#fig2.suptitle('Statistics scatter for r=(%.2f$\pm$%.2f), taken from %.1i shots with a std of %.2f. \n In red is shown the mean and the standar deviation.' %(r, r_std,num_points,std), fontsize=11, fontweight='bold')
#ax2=fig2.add_subplot(1,1,1)
##plt.contourf(bval,M,bcount)
#plt.pcolor(bval_flat,M_flat,bcount_flat)
#plt.plot(avg_stat_bin,test_points,'--r',alpha=0.8)
#ax2.set_xlabel('$y*$ at $t=$%.1f' %(t_fin))
#ax2.set_ylabel('$<y_0>$')
#plt.colorbar()

