# -*- coding: utf-8 -*-
"""
Created on Mon Aug 10 19:10:48 2020

@author: gomel
"""

import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint
import threshold
import metrics as metric
import astropy.stats as astrost
#import astropy.stats.knuth_bin_width as knuth
from scipy.interpolate import griddata
from mpl_toolkits.mplot3d import Axes3D

def pcolor_plot_free(x):
        
        
    method='knuth'
    bin_num=60
    
    '''set up  pcolor figures with std'''
    std=0.1
    lims=0.4
    side=6
    amount_of_stats=2*side+1 #○so it always includes 0
    binss=np.linspace(-0.6,0.6,bin_num+1)
    
    avg_stat_bin=np.zeros((amount_of_stats))
    std_stat_bin=np.zeros((amount_of_stats))
    testlim=.50
    test_points=np.linspace(-testlim,testlim,amount_of_stats)
    r=0.1
    r_std=0.03
    j=0
    num_points=3000
    
    M=[]
    bcount=[]
    bval=[]
    
    t_fin=30

    for n,mean in enumerate(test_points):
        j=j+1 
        print(mean)
        initials=[]
        rs=[]
        results=[]
        for i in range(0,num_points):
            ys=x
            ys = np.array(ys).flatten()
            results.append(ys[-1])
    
        res_ct=astrost.histogram(results,bins=bin_num,density=True)
        res_ct2=astrost.histogram(results,bins=method,density=True)
        
    num=res_ct[0]


    res_ct=astrost.histogram(results,bins=method,density=True)
    bcount.append(res_ct[0])
    bval.append(res_ct[1][:-1])
    M.append(mean*np.ones_like(res_ct[1][:-1]))
    avg_stat_bin[n]=np.mean(results)
    
    binss=np.linspace(-0.6,0.6,bin_num+1)
    testlim=.50

    
    bval_flat=[item for sublist in bval for item in sublist]
    M_flat=[item for sublist in M for item in sublist]
    bcount_flat=[item for sublist in bcount for item in sublist]
    
    xi = np.linspace(min(binss),max(binss),150)
    yi = np.linspace(-testlim,testlim,150)
    zi = griddata((bval_flat,M_flat ),bcount_flat, (xi[None,:], yi[:,None]), method='cubic')
    
    fig2=plt.figure(figsize=(10,10))
    fig2.suptitle('Statistics scatter for r=(%.2f$\pm$%.2f), taken from %.1i shots with a std of %.2f. \n In red is shown the mean and the standar deviation.' %(r, r_std,num_points,std), fontsize=11, fontweight='bold')
    ax2=fig2.add_subplot(1,1,1)
    #plt.contourf(bval,M,bcount)
    #plt.contour(xi,yi,zi)
    plt.pcolor(xi,yi,zi)
    #plt.pcolor(xi,yi,np.log(zi))
    plt.plot(avg_stat_bin,test_points,'--r',alpha=0.8)
    ax2.set_xlabel('$y*$ at $t=$%.1f' %(t_fin))
    ax2.set_ylabel('$<y_0>$')
    plt.colorbar()    
    
    return binss