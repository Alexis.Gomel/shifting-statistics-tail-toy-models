# -*- coding: utf-8 -*-
"""
Created on Tue Aug 18 10:21:23 2020

@author: gomel
"""


import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint
import threshold
import metrics as metric
import astropy.stats as astrost
#import astropy.stats.knuth_bin_width as knuth
from scipy.interpolate import griddata
from mpl_toolkits.mplot3d import Axes3D
import datetime as dt
import os

def f_p(y,t,r):
    return r*y-y**3


'''parameters'''
std=0.12
r=1
r_std=0.1
t_fin=9
y0=0.150

lims=0.4
testlim=.45
num_points=100

rs=np.linspace(-1,3,300)

sol=np.zeros_like(rs)
sol[rs>0]=np.sqrt(rs[rs>0])

rx1=np.linspace(r-0.5,r+0.5,300)
r_example=0.3*1/(r_std*2*np.pi)*np.exp(-0.5*((rx1-r)/r_std)**2)


y1=np.linspace(y0-0.3,y0+0.3,300)
y_example=0.3*1/(std*2*np.pi)*np.exp(-0.5*((y1-y0)/std)**2)+max(rs)-0.2


yrandom=np.zeros([num_points])
xrandom=np.zeros([num_points])
M=np.zeros([num_points])

        
for n in range(num_points):
    xs = np.linspace(0,t_fin,2)
    yinit = np.random.normal(y0,std,2)  # the initial condition
    yrandom[n]=yinit[-1]
    param=(np.random.normal(r,r_std,1),)
    xrandom[n]=param[0]
    ys = odeint(f_p, yinit[-1],xs,param)
    M[n]=ys[-1]
    




fig=plt.figure(figsize=(12,5))
ax=fig.add_subplot(121)
plt.plot(rs,sol,color='grey',linewidth=3)
plt.plot(rs,-sol,color='grey',linewidth=3)
plt.plot(rs[rs>0],0*rs[rs>0],'--',color='grey',linewidth=0.7)
plt.plot(rx1,r_example+y0,'b')
plt.plot([r,r],[min(-sol),max(r_example)],'--b',alpha=0.3)
plt.plot(y_example,y1,color='orange')
plt.plot([min(rs),max(y_example)],[y0,y0],'--',color='orange',alpha=0.3)
plt.annotate('',
             [max(y_example)+0.09,y0-0.4],fontsize='x-large',
             xytext=(max(y_example)+0.09, y0+0.4) ,
             arrowprops=dict(arrowstyle="<->"),va="center")
ax.scatter(xrandom,yrandom,color='orange',alpha=0.6)
ax.scatter(xrandom,M,color='red',alpha=0.6)
plt.xlabel('r')
plt.ylabel('y*')
plt.yticks([y0], ['$<\!y_0\!>$'],fontsize='large')  # Set label locations.
plt.xticks([0,r], ['$0$','$<\!r\!>$'],fontsize='large')  # Set label locations.
# plt.tight_layout()


#%%
res_ct=astrost.histogram(M,bins='knuth')
       
ax=fig.add_subplot(122)
height,bin_pos=astrost.histogram(M,bins='knuth')
ax.bar(bin_pos[:-1],height,width=np.diff(bin_pos),color='red',align='edge',alpha=0.5,label='Solutions')
# ax.plot(res_ct[1][:-1],res_ct[0],color='red',alpha=0.4)
plt.plot([np.sqrt(r),np.sqrt(r)],[min(res_ct[0]),max(res_ct[0])],'--r',alpha=0.9)
plt.plot([-np.sqrt(r),-np.sqrt(r)],[min(res_ct[0]),max(res_ct[0])],'--r',alpha=0.9)
plt.plot([0,0],[min(res_ct[0]),max(res_ct[0])],':k',alpha=0.9)

height,bin_pos=astrost.histogram(yrandom,bins='knuth')
ax.bar(bin_pos[:-1],height,width=np.diff(bin_pos),color='orange',align='edge',alpha=0.5,label='Initial conditions')
plt.plot([y0,y0],[min(res_ct[0]),max(res_ct[0])],'--',color='orange',alpha=0.9)
plt.xticks([-np.sqrt(r),y0,np.sqrt(r)], ['$-\sqrt{<\!r\!>}$','$<\!y_0\!>$','$\sqrt{<\!r\!>}$'],fontsize='medium')  # Set label locations.
plt.yticks([])  # Set label locations.
plt.xlabel('y*')
plt.legend()

plt.tight_layout()

# plt.plot(rs,sol,color='grey',linewidth=3)
# plt.plot(rs,-sol,color='grey',linewidth=3)
# plt.plot(rs[rs>0],0*rs[rs>0],'--',color='grey',linewidth=0.7)
# plt.plot(rx1,r_example+y0,'b')
# plt.plot([r,r],[min(-sol),max(r_example)],'--b',alpha=0.3)
# plt.plot(y_example,y1,color='orange')
# plt.plot([min(rs),max(y_example)],[y0,y0],'--',color='orange',alpha=0.3)
# plt.annotate('',
#              [max(y_example)+0.09,y0-0.4],fontsize='x-large',
#              xytext=(max(y_example)+0.09, y0+0.4) ,
#              arrowprops=dict(arrowstyle="<->"),va="center")
# ax.scatter(xrandom,yrandom,color='orange',alpha=0.6)
# ax.scatter(xrandom,M,color='red',alpha=0.6)
# plt.xlabel('r')
# plt.ylabel('y*')
# plt.yticks([y0], ['$<\!y_0\!>$'],fontsize='large')  # Set label locations.
# plt.xticks([0,r], ['$0$','$<\!r\!>$'],fontsize='large')  # Set label locations.
# plt.tight_layout()