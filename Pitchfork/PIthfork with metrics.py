# -*- coding: utf-8 -*-
"""
Created on Sun Jul 28 21:36:01 2019

Pithfork test with metrics:
    
@author: gomel
"""




import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint
import threshold
import metrics 
import astropy.stats as astrost
import boots

def f_p(y,t,r):
#    r=2.0
    return r*y-y**3

r=0.2
y0=1.2
xs = np.linspace(0,50,100)
    #    y0 = 10.0  # the initial condition
param=(r,)
ys = odeint(f_p, y0,xs,param)
ys = np.array(ys).flatten()



'''set up pcolor figures with percentiles'''
fig2=plt.figure(figsize=(10,10))
ax2=fig2.add_subplot(1,1,1)

std=0.1
lims=0.4
amount_of_stats=15
binss=np.linspace(-0.6,0.6,70)

pcolor_matrix=np.zeros((amount_of_stats,len(binss)-1))
pcolor_logmatrix=np.zeros((amount_of_stats,len(binss)-1))
avg_stat_bin=np.zeros((amount_of_stats))
perc_stat_bin_below=np.zeros((amount_of_stats))
perc_stat_bin_above=np.zeros((amount_of_stats))
perc=90#has to be grater than 50

testlim=.35
test_points=np.linspace(-testlim,testlim,amount_of_stats)
r=0.1
r_std=0.03
j=0
num_points=1000
fig2.suptitle('Statistics pcolor for r=(%.2f$\pm$%.2f), taken from %.1i shots with a std of %.2f. \n In red is shown the mean and the %.1f and %.1f percentiles ' %(r, r_std,num_points,std,perc,100-perc), fontsize=11, fontweight='bold')

t_fin=40
for n,mean in enumerate(test_points):
    j=j+1 
    print(mean)
    initials=[]
    rs=[]
    results=[]
    for i in range(0,num_points):
        xs = np.linspace(0,t_fin,2)
        y0 = np.random.normal(mean,std,2)  # the initial condition
        param=(np.random.normal(r,r_std,1),)
        ys = odeint(f_p, y0[-1],xs,param)
        ys = np.array(ys).flatten()
        results.append(ys[-1])

    num,bin_pos=astrost.histogram(results,bins='scott')

    pcolor_matrix[n,:]=num/np.max(num)
    pcolor_logmatrix[n,:]=np.log10(num/np.max(num))
    avg_stat_bin[n]=np.median(results)
    perc_stat_bin_below[n]=np.percentile(results,100-perc)
    perc_stat_bin_above[n]=np.percentile(results,perc)
    

ax2.set_xlabel('$y*$ at $t=$%.1f' %(t_fin))
ax2.set_ylabel('$<y_0>$')

cmap=ax2.pcolor(binss[:-1],test_points,pcolor_logmatrix)
fig2.colorbar(cmap)
#ax2.plot(avg_stat_bin,test_points,'--r',alpha=0.6,label='mean')
#ax2.fill_betweenx(test_points,perc_stat_bin_below,perc_stat_bin_above,color='red',alpha=0.25)
ax2.legend()