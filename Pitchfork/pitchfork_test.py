# -*- coding: utf-8 -*-
"""
Created on Tue Jul  9 13:59:33 2019.
Pitchfork from intial conditions. 
2d plots with percentiles and std. 
@author: gomel
"""

import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint
import threshold
import metrics as metric
import astropy.stats as astrost
#import astropy.stats.knuth_bin_width as knuth
from scipy.interpolate import griddata
import datetime as dt
import os

now = dt.datetime.now()
# dd/mm/YY H:M:S
dt_string = now.strftime("%d_%m_%Y-%H_%M_%S")

def f_p(y,t,r):
#    r=2.0
    return r*y-y**3

r=0.2
r_std=0.03

y0=1.2
xs = np.linspace(0,50,100)
    #    y0 = 10.0  # the initial condition
param=(r,)
ys = odeint(f_p, y0,xs,param)
ys = np.array(ys).flatten()

fig=plt.figure()
ax=fig.add_subplot(1,1,1)
ax.plot(xs,ys)

#%%
fig=plt.figure()
ax=fig.add_subplot(1,1,1)

rs=[]
results=[]
for r in np.linspace(-2.,2,1000):
    for y0 in np.linspace(-0.5,0.5,10):
        xs = np.linspace(0,50,100)
    #    y0 = 10.0  # the initial condition
        param=(r,)
        ys = odeint(f_p, y0,xs,param)
        ys = np.array(ys).flatten()
        results.append(ys[-1])
        rs.append(r)
ax.plot(rs,results,'.')
ax.set_xlabel('$r$')
ax.set_ylabel('$y* $')
ax.plot(np.linspace(0,2,30),np.zeros((30)),'--r')

#%%
#test randomness

rs=[]
results=[]
mean=0.1
std=.5

fig=plt.figure()
ax1=fig.add_subplot(1,2,1)
ax2=fig.add_subplot(1,2,2)

initial_points=[]
ax2.hist(np.random.normal(mean,std,1000),color='orange')
ax2.set_xlabel('Initial conditions $(y_0)$')
for r in np.linspace(-1.,1,1000):
    for i in range(0,40):
        xs = np.linspace(0,50,100)
        y0 = np.random.normal(mean,std,1)  # the initial condition
        param=(np.random.normal(r,r_std,1),)
        ys = odeint(f_p, y0,xs,param)
        ys = np.array(ys).flatten()
        results.append(ys[-1])
        rs.append(r)

ax1.plot(rs,results,'.')
size=40
ax1.plot(np.ones((size))*rs[-1]*1.1,np.random.normal(mean,std,size),'.')
ax1.set_xlabel('r')
ax1.set_ylabel('y*')

#%%
"""STATISTIC PLOTS"""

std=0.1
lims=0.5
amount_of_stats=7
testlim=.35
test_points=np.linspace(-testlim,testlim,7)
#test_points=[-1,-0.5,0,0.5,1,20]
r=0.1


fig2=plt.figure()
ax1=fig2.add_subplot(len(test_points),1,1)
j=0


num_points=800
for mean in test_points:
    j=j+1 
    print(mean)
    initials=[]
    rs=[]
    results=[]
    for i in range(0,num_points):
        xs = np.linspace(0,40,2)
        y0 = np.random.normal(mean,std,2)  # the initial condition
        param=(np.random.normal(r,r_std,1),)
        ys = odeint(f_p, y0[-1],xs,param)
        ys = np.array(ys).flatten()
        results.append(ys[-1])
        initials.append(y0[-1])
    
    ax1=fig2.add_subplot(len(test_points),1,j)
    if j<7:
        ax1.axes.get_xaxis().set_visible(False)
        # res_ct=hist(results,bins=60,density=True)
    ax1.hist(results,bins=60,density=True)
    ax1.hist(initials,density=True,alpha=0.4,label='$<y_0>$='+str(mean))
    
    ax1.set_xlim((-1.3*lims,1.3*lims))
#    ax1.set_yscale('log')
    ax1.legend()
    
ax1.set_xlabel('$y*$')

#%%
'''set up  pcolor figures with std'''
fig6=plt.figure(figsize=(10,10))
fig2=plt.figure(figsize=(10,10))
ax2=fig2.add_subplot(1,1,1)
ax6=fig6.add_subplot(1,1,1)

std=0.1
lims=0.4
amount_of_stats=15
binss=np.linspace(-0.6,0.6,70)

pcolor_matrix=np.zeros((amount_of_stats,len(binss)-1))
pcolor_logmatrix=np.zeros((amount_of_stats,len(binss)-1))
avg_stat_bin=np.zeros((amount_of_stats))
std_stat_bin=np.zeros((amount_of_stats))
testlim=.35
test_points=np.linspace(-testlim,testlim,amount_of_stats)
r=0.1
r_std=0.03
j=0
num_points=1000
fig2.suptitle('Statistics pcolor for r=(%.2f$\pm$%.2f), taken from %.1i shots with a std of %.2f. \n In red is shown the mean and the standar deviation.' %(r, r_std,num_points,std), fontsize=11, fontweight='bold')

t_fin=40
for n,mean in enumerate(test_points):
    j=j+1 
    print(mean)
    initials=[]
    rs=[]
    results=[]
    for i in range(0,num_points):
        xs = np.linspace(0,t_fin,2)
        y0 = np.random.normal(mean,std,2)  # the initial condition
        param=(np.random.normal(r,r_std,1),)
        ys = odeint(f_p, y0[-1],xs,param)
        ys = np.array(ys).flatten()
        results.append(ys[-1])

    num, bins, patches=ax6.hist(results, binss,alpha=0.9)
    pcolor_matrix[n,:]=num/np.max(num)
    pcolor_logmatrix[n,:]=np.log(num/np.max(num))
    avg_stat_bin[n]=np.mean(results)
    std_stat_bin[n]=np.std(results)

ax2.set_xlabel('$y*$ at $t=$%.1f' %(t_fin))
ax2.set_ylabel('$<y_0>$')

cmap=ax2.pcolor(binss,test_points,pcolor_logmatrix)
fig2.colorbar(cmap)
ax2.plot(avg_stat_bin,test_points,'--r',alpha=0.6,label='mean')
ax2.fill_betweenx(test_points,avg_stat_bin-std_stat_bin,avg_stat_bin+std_stat_bin,color='red',alpha=0.25)
ax2.legend()

#%%
'''set up pcolor figures with percentiles'''
fig6=plt.figure(figsize=(10,10))
fig2=plt.figure(figsize=(10,10))
ax2=fig2.add_subplot(1,1,1)
ax6=fig6.add_subplot(1,1,1)

std=0.1
lims=0.4
amount_of_stats=15
binss=np.linspace(-0.6,0.6,70)

pcolor_matrix=np.zeros((amount_of_stats,len(binss)-1))
pcolor_logmatrix=np.zeros((amount_of_stats,len(binss)-1))
avg_stat_bin=np.zeros((amount_of_stats))
perc_stat_bin_below=np.zeros((amount_of_stats))
perc_stat_bin_above=np.zeros((amount_of_stats))
perc=90#has to be grater than 50

testlim=.35
test_points=np.linspace(-testlim,testlim,amount_of_stats)
r=0.1
r_std=0.03
j=0
num_points=1000
fig2.suptitle('Statistics pcolor for r=(%.2f$\pm$%.2f), taken from %.1i shots with a std of %.2f. \n In red is shown the mean and the %.1f and %.1f percentiles ' %(r, r_std,num_points,std,perc,100-perc), fontsize=11, fontweight='bold')

t_fin=40
for n,mean in enumerate(test_points):
    j=j+1 
    print(mean)
    initials=[]
    rs=[]
    results=[]
    for i in range(0,num_points):
        xs = np.linspace(0,t_fin,2)
        y0 = np.random.normal(mean,std,2)  # the initial condition
        param=(np.random.normal(r,r_std,1),)
        ys = odeint(f_p, y0[-1],xs,param)
        ys = np.array(ys).flatten()
        results.append(ys[-1])

    num, bins, patches=ax6.hist(results, binss,alpha=0.9)
    pcolor_matrix[n,:]=num/np.max(num)
    pcolor_logmatrix[n,:]=np.log10(num/np.max(num))
    avg_stat_bin[n]=np.mean(results)
    perc_stat_bin_below[n]=np.percentile(results,100-perc)
    perc_stat_bin_above[n]=np.percentile(results,perc)
    

ax2.set_xlabel('$y*$ at $t=$%.1f' %(t_fin))
ax2.set_ylabel('$<y_0>$')

cmap=ax2.pcolor(binss,test_points,pcolor_logmatrix)
fig2.colorbar(cmap)
#ax2.plot(avg_stat_bin,test_points,'--r',alpha=0.6,label='mean')
ax2.fill_betweenx(test_points,perc_stat_bin_below,perc_stat_bin_above,color='red',alpha=0.25)
ax2.legend()

"""testing metrics!!!"""



#%%
"""STATISTIC PLOTS"""

std=0.005
lims=0.5
amount_of_stats=7
testlim=.35
test_points=np.linspace(-testlim,testlim,7)
#test_points=[-1,-0.5,0,0.5,1,20]
r=0.1


fig2=plt.figure()
ax1=fig2.add_subplot(len(test_points),1,1)
j=0
method='knuth'
bin_num=60

sigma=0.03

num_points=5000
fig2.suptitle('Comparison between method'+method+'\n and constant bining with %.1i bins from %.1i points'%(bin_num,num_points), fontsize=11, fontweight='bold')

for mean in test_points:
    j=j+1 
    print(mean)
    initials=[]
    rs=[]
    results=[]
    for i in range(0,num_points):
        xs = np.linspace(0,40,2)
        y0 = np.random.normal(mean,std,2)  # the initial condition
        param=(np.random.normal(r,sigma,1),)
        ys = odeint(f_p, y0[-1],xs,param)
        ys = np.array(ys).flatten()
        results.append(ys[-1])
        initials.append(y0[-1])
    
    ax1=fig2.add_subplot(len(test_points),1,j)
    if j<7:
        ax1.axes.get_xaxis().set_visible(False)
        res_ct=astrost.histogram(results,bins=bin_num,density=True)
        res_ct2=astrost.histogram(results,bins=method,density=True)
    ax1.plot(res_ct[1][:-1],res_ct[0])
    ax1.plot(res_ct2[1][:-1],res_ct2[0])
#    ax1.hist(initials,density=True,alpha=0.4,label='$<y_0>$='+str(mean))
    ax1.set_xlim((-1.3*lims,1.3*lims))
#    ax1.set_ylim((0,10))

#    ax1.set_yscale('log')
    ax1.legend()
    
ax1.set_xlabel('$y*$')


'''save parameters'''
#get matfiles
files=os.listdir()
savefile='.\\'+dt_string

os.makedirs(savefile)

'''print txt file with analysis parameters'''
analysis_txt=open(savefile+r"\analysis parameters.txt",'w+')
analysis_txt.write("Parameters for the analyzed data:"+'\n\n')
analysis_txt.write("r: %.2f+\n" %(r))
analysis_txt.write("r_std: %.2f+\n" %(r_std))
analysis_txt.write("t0: %.2f+\n" %(r))
